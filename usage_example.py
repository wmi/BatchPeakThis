# -*- coding: utf-8 -*-
from __future__ import absolute_import
from guidata.qt import QtGui
from bbfmr.measurement.vna import VNAMeasurement
import bbfmr.processing as bp
from peakit.controller.MeasurementController import MeasurementController

def run(m):
     app = QtGui.QApplication()
     main_controller = MeasurementController(measurement=m)
     main_controller.show_view()
     app.exec_()

     return main_controller

#%% Load data from tdms file
fname_tdms = r"./path_to_your_measurement_file.tmds"
m = VNAMeasurement(fname_tdms)
m.metadata["title"] = fname_tdms
m.metadata["xlabel"] = "f"
m.metadata["ylabel"] = "B"
m.metadata["zlabel"] = "S21"   

# %% Add processing steps
m.add_operation(bp.swap_axes) # swap B and f axis

# %% Run graphical fitting interface
main_controller = run(m=m)