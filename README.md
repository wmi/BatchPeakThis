# BatchPeakThis
BatchPeakThis is a customized version of PeakThis [1] that is used to fit 
slices of measurement objects of pybbfmr [2]. 
Additionally, it can fit complex-valued models to complex valued data.

Parts of the readme of PeakThis can be found below; BatchPeakThis is published
under the same license (GPL, see LICENSE file ) as PeakThis.

A simple usage example can be found in usage_example.py .

 * [1] https://github.com/CPrescher/PeakThis
 * [2] https://gitlab.com/wmi/pybbfmr


## Requirements

    * python > 3.4
    * PyQt4/PyQt5
    * numpy
    * scipy
    * lmfit
    * pyqtgraph
    * bbfmr
    * guidata (at the moment, for easy PyQt4/PyQt5 compatibility)
    
It is known tu run on Windows and Linux.

## Maintainer
Hannes Maier-Flaig (hannes.maier-flaig@wmi.badw.de))


# PeakThis

Highly customizable cross-platform peak fitting software

## Maintainer

Clemens Prescher (clemens.prescher@gmail.com)

  
    