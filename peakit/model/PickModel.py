# -*- coding: utf8 -*-
__author__ = 'Clemens Prescher'

#from copy import deepcopy


class PickModel(object):
    num_pick_models = 0

    def __init__(self, number_picks, prefix=None):
        self.id = PickModel.num_pick_models
        PickModel.num_pick_models += 1

        if not prefix:
            self._prefix = "id%d_" % self.id
        else:
            self._prefix = prefix

        self.current_pick = 0
        self.number_picks = number_picks
        self.pick_points = [Point()] * number_picks

    def update_current_parameter(self, x, y):
        raise NotImplementedError

    def pick_parameter(self, x, y):
        self.update_current_parameter(x, y)
        self.pick_points[self.current_pick].x = x
        self.pick_points[self.current_pick].y = y

        self.current_pick += 1
        if self.current_pick < self.number_picks:
            return True
        else:
            self.current_pick = 0
            return False

    def get_param(self, param_name):
        return self.parameters[self.prefix + param_name]

    def set_parameter_value(self, param_name, value):
        self.parameters[self.prefix + param_name].value = value

    def get_parameter_value(self, param_name):
        return self.parameters[self.prefix + param_name].value

    def set_parameter_max_value(self, param_name, max_value):
        self.parameters[self.prefix + param_name].max = max_value

    def get_parameter_max_value(self, param_name):
        return self.parameters[self.prefix + param_name].max

    def set_parameter_min_value(self, param_name, min_value):
        self.parameters[self.prefix + param_name].min = min_value

    def get_parameter_min_value(self, param_name):
        return self.parameters[self.prefix + param_name].min
        
    def set_prefix(self, new_prefix):
        old_prefix = self._prefix
        self._prefix = new_prefix
        
        self._param_names = [pname.replace(old_prefix, new_prefix) for pname in self._param_names]
        
        new_params = self.make_params()
        print(new_params)
        for param_name in self._param_root_names:
            result_param = new_params[new_prefix + param_name] 
            current_param = self.parameters[old_prefix + param_name]

            result_param.value = current_param.value
            result_param.vary = current_param.vary
            result_param.min = current_param.min
            result_param.max = current_param.max
            
        self.parameters = new_params
        

    def quick_eval(self, x):
        if len(x) == 0:
            return []
        return self.eval(self.parameters, x=x)

    def eval(self, *args, **kwargs):
        raise NotImplementedError

    def make_params(self, *args, **kwargs):
        raise NotImplementedError

    def __deepcopy__(self, memo):
        cls = self.__class__
        result = cls()

        result._prefix = result.prefix
        result.make_params()
        for parameter_name in self._param_root_names:
            result_param = result.get_param(parameter_name)
            current_param = self.get_param(parameter_name)

            result_param.value = current_param.value
            result_param.vary   = current_param.vary
            result_param.min = current_param.min
            result_param.max = current_param.max

        return result


class Point():
    def __init__(self, x=0., y=0.):
        self.x = x
        self.y = y