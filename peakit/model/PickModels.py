# -*- coding: utf8 -*-
from lmfit.models import (Model, ConstantModel, LinearModel, QuadraticModel,
                          GaussianModel, LorentzianModel)
from bbfmr.models.complex import ComplexLinearModel
from bbfmr.models.susceptibility import (VNAFMR_EllipsoidSphereModel,
                                         VNAFMR_EllipsoidDerivativeSphereModel,
                                         VNAFMR_EllipsoidDifferenceQuotientSphereModel,
                                         VNAFMR_IPFieldModel)
from bbfmr.models.dispersion import FreqKittelIPModel, FreqKittelOOPModel

from collections import OrderedDict
import numpy as np

from peakit.model.PickModel import PickModel
s2pi = np.sqrt(2*np.pi)
mu_0 = 4*np.pi*1e-7

class PickConstantModel(ConstantModel, PickModel):
    def __init__(self, *args, prefix=None, **kwargs):
        PickModel.__init__(self, 1, prefix=prefix)
        super(PickConstantModel, self).__init__(prefix=self._prefix,*args, **kwargs)
        self.parameters = self.make_params()

    def update_current_parameter(self, x, y):
        self.set_parameter_value('c', y)


class PickLinearModel(LinearModel, PickModel):
    def __init__(self, *args, prefix=None, **kwargs):
        PickModel.__init__(self, 2, prefix=prefix)
        super(PickLinearModel, self).__init__(prefix=self._prefix, *args, **kwargs)
        self.parameters = self.make_params()

        self.set_parameter_value('intercept', 0)
        self.set_parameter_value('slope', 0)

    def update_current_parameter(self, x, y):
        if self.current_pick == 0:
            self.set_parameter_value('intercept', y)
        elif self.current_pick == 1:
            try:
                slope = (y - self.pick_points[0].y) / \
                        (x - self.pick_points[0].x)
                self.set_parameter_value('slope', slope)
                self.set_parameter_value('intercept',  y - slope * x)
            except ZeroDivisionError:
                pass


class PickQuadraticModel(QuadraticModel, PickModel):
    def __init__(self, *args, prefix=None, **kwargs):
        PickModel.__init__(self, 3, prefix=prefix)
        super(PickQuadraticModel, self).__init__(prefix=self._prefix,*args, **kwargs)

        self.parameters = self.make_params()
        self.set_parameter_value('a', 0)
        self.set_parameter_value('b', 0)
        self.set_parameter_value('c', 0)

        # parameters for defining the initial fit
        self.x = np.array([0., 0., 0.])
        self.y = np.array([0., 0., 0.])

    def update_current_parameter(self, x, y):

        self.x[self.current_pick] = float(x)
        self.y[self.current_pick] = float(y)

        if self.current_pick == 0:
            self.set_parameter_value('c', y)
        elif self.current_pick == 1:
            slope = (self.y[1] - self.y[0]) / (self.x[1] - self.x[0])
            self.set_parameter_value('b', slope)
            self.set_parameter_value('c', self.y[0] - slope * self.x[0])
        elif self.current_pick == 2:
            try:
                a, b, c = np.polyfit(self.x, self.y, 2)
                self.set_parameter_value('a', a)
                self.set_parameter_value('b', b)
                self.set_parameter_value('c', c)
            except (np.linalg.LinAlgError, ValueError):
                self.set_parameter_value('a', 0)


def gaussian(x, center=0.0, fwhm=1.0, intensity=1):
    """1 dimensional gaussian:
    gaussian(x, amplitude, center, sigma)
    """
    hwhm = fwhm/2.0
    return intensity*0.8326/(hwhm*1.7725)*np.exp(-(x-center)**2/(hwhm/0.8326)**2)


class PickGaussianModel(GaussianModel, PickModel):
    def __init__(self, *args, prefix=None, **kwargs):
        PickModel.__init__(self, 2, prefix=prefix)
        super(PickGaussianModel, self).__init__(prefix=self._prefix, *args, **kwargs)
        self.parameters = self.make_params()

        self.set_parameter_value('amplitude', 0)
        self.set_parameter_value('center', 0)
        self.set_parameter_value('fwhm', 0.5)

    def update_current_parameter(self, x, y):
        if self.current_pick == 0:
            self.set_parameter_value('center', x)
            # fwhm = self.parameters['sigma'].value*2.354820
            self.set_parameter_value('amplitude', y * 1.7724538509055159*self.get_parameter_value('sigma')/1.6652)
        elif self.current_pick == 1:
            new_fwhm = abs(x - self.get_parameter_value('center')) * 2
            if new_fwhm == 0:
                new_fwhm = 0.5
            self.set_parameter_value('sigma', new_fwhm)
            self.set_parameter_value('amplitude', self.pick_points[0].y * \
                                     1.7724538509055159*self.get_parameter_value('sigma')/1.6652)        


def lorentzian(x, center=0, fwhm=0.3, intensity=1):
    hwhm = fwhm*0.5
    return intensity/(np.pi*hwhm*(1+((x-center)/hwhm)**2))


class LorentzianPickModel(LorentzianModel, PickModel):
    def __init__(self, *args, prefix=None, **kwargs):
        PickModel.__init__(self, 2, prefix=prefix)
        super(LorentzianPickModel, self).__init__(prefix=self._prefix, *args, **kwargs)

        self.parameters = self.make_params()
        self.set_parameter_value('amplitude', 0)
        self.set_parameter_value('center', 0)
        self.set_parameter_value('sigma', 0.5)

    def update_current_parameter(self, x, y):
        if self.current_pick == 0:
            self.set_parameter_value('center', x)
            self.set_parameter_value('amplitude', y * np.pi*self.get_parameter_value('sigma')/2.)
        elif self.current_pick == 1:
            new_fwhm = abs(x - self.get_parameter_value('center'))*2
            if new_fwhm == 0:
                new_fwhm = 0.5
            self.set_parameter_value('sigma', new_fwhm)
            self.set_parameter_value('amplitude', self.pick_points[0].y * np.pi*self.get_parameter_value('sigma')/2.)

    def recommended_roi(self, x, data, arg):
        p = self.guess(data, x=x)
        sigma = p[self.prefix + "sigma"].value * float(arg)
        center = p[self.prefix + "center"].value
        min_idx = np.argmin(np.abs(x - (center - sigma)))
        max_idx = np.argmin(np.abs(x - (center + sigma)))
        
        return np.arange(min_idx, max_idx)
    
def pseudo_voigt(x, center=0, fwhm=0.3, intensity=1, n=0.5):
    return n*lorentzian(x, center, fwhm, intensity)+(1-n)*gaussian(x, center, fwhm, intensity)


class PseudoVoigtModel(Model):
    __doc__ = lorentzian.__doc__
    def __init__(self, *args, **kwargs):
        super(PseudoVoigtModel, self).__init__(pseudo_voigt, *args, **kwargs)


class PseudoVoigtPickModel(PseudoVoigtModel, PickModel):
    def __init__(self, *args, prefix=None, **kwargs):
        PickModel.__init__(self, 2, prefix=prefix)
        super(PseudoVoigtPickModel, self).__init__(prefix=self._prefix, *args, **kwargs)

        self.parameters = self.make_params()
        self.set_parameter_value('intensity', 0)
        self.set_parameter_value('center', 0)
        self.set_parameter_value('fwhm', 0.5)
        self.set_parameter_value('n', 0.5)
        self.set_parameter_min_value('n', 0)
        self.set_parameter_max_value('n', 1)

    def update_current_parameter(self, x, y):
        if self.current_pick == 0:
            self.set_parameter_value('center', x)
            self.set_parameter_value('intensity', y * np.pi*self.get_parameter_value('fwhm') *0.25  + \
                                                  y * 1.7724538509055159*self.get_parameter_value('fwhm')/1.6652 * 0.5)
        elif self.current_pick == 1:
            new_fwhm = abs(x - self.get_parameter_value('center'))*2
            if new_fwhm==0:
                new_fwhm = 0.5
            self.set_parameter_value('fwhm', new_fwhm)
            self.set_parameter_value('intensity',
                                     self.pick_points[0].y * np.pi*self.get_parameter_value('fwhm') *0.25  + \
                                     self.pick_points[0].y * 1.7724538509055159*self.get_parameter_value('fwhm')/1.6652 * 0.5)


class Pick_ComplexLinearModel(ComplexLinearModel, PickModel):
    def __init__(self, *args, prefix=None, **kwargs):
        PickModel.__init__(self, 4, prefix=prefix)
        super().__init__(prefix=self._prefix, **kwargs)
        self.parameters = self.make_params()

    def update_current_parameter(self, x, y):
        raise NotImplementedError

class Pick_VNAFMR_EllipsoidDerivativeSphereModel(VNAFMR_EllipsoidDerivativeSphereModel, PickModel):
    def __init__(self, *args, prefix=None, **kwargs):
        PickModel.__init__(self, 4, prefix=prefix)
        super().__init__(prefix=self._prefix, **kwargs)
        self.parameters = self.make_params()

    def update_current_parameter(self, x, y):
        if self.current_pick == 0:
            self.set_parameter_value('f_r', x)
            self.set_parameter_value('Z', self.calc_Z(y, self.parameters))
        elif self.current_pick == 1:
            df = abs(x - self.get_parameter_value('f_r'))
            self.set_parameter_value('df', df)
            height= abs(y - self.pick_points[0].y)
            if height != 0:
                self.set_parameter_value('Z',self.calc_Z(height, self.parameters))

class Pick_VNAFMR_EllipsoidSphereModel(VNAFMR_EllipsoidSphereModel, PickModel):
    def __init__(self, *args, prefix=None, **kwargs):
        PickModel.__init__(self, 3, prefix=prefix)
        super().__init__(prefix=self._prefix, **kwargs)
        self.parameters = self.make_params()

    def update_current_parameter(self, x, y):
        if self.current_pick == 0:
            self.set_parameter_value('f_r', x)
            self.set_parameter_value('Z', self.calc_Z(y, self.parameters))
        elif self.current_pick == 1:
            df = abs(x - self.get_parameter_value('f_r'))
            self.set_parameter_value('df', df)
            height= abs(y - self.pick_points[0].y)
            if height != 0:
                self.set_parameter_value('Z',self.calc_Z(height, self.parameters))
        elif self.current_pick == 2:
            dist = x - self.get_parameter_value('f_r')
            if dist != 0:
                phi = dist/self.parameters[self.prefix + "df"].value * np.pi/2/2
                self.parameters[self.prefix + "phi"].value = phi
                
     
class Pick_VNAFMR_EllipsoidDifferenceQuotientSphereModel(VNAFMR_EllipsoidDifferenceQuotientSphereModel, PickModel):
    def __init__(self, *args, prefix=None, **kwargs):
        PickModel.__init__(self, 4, prefix=prefix)
        super().__init__(prefix=self._prefix, **kwargs)
        self.parameters = self.make_params()

    def update_current_parameter(self, x, y):
        if self.current_pick == 0:
            self.set_parameter_value('f_r', x)
            self.set_parameter_value('Z', self.calc_Z(y, self.parameters))
        elif self.current_pick == 1:
            df = abs(x - self.get_parameter_value('f_r'))
            self.set_parameter_value('df', df)
            height= abs(y - self.pick_points[0].y)
            if height != 0:
                self.set_parameter_value('Z',self.calc_Z(height, self.parameters))
           


#class Pick_VNAFMR_EllipsoidDerivativeOOPQuickModel(VNAFMR_EllipsoidDerivativeModel, PickModel):
#    def __init__(self, *args, prefix=None, **kwargs):
#        PickModel.__init__(self, 3, prefix=prefix)
#        super().__init__(prefix=self._prefix, **kwargs)
#        self.parameters = self.make_params()
#        self.N = (0, 0, 1)
#
#    def update_current_parameter(self, x, y):
#        if self.current_pick == 0:
#            self.set_parameter_value('f_r', x)
#            self.set_parameter_value('Z', self.calc_Z(y, self.parameters))
#        elif self.current_pick == 1:
#            df = abs(x - self.get_parameter_value('f_r'))
#            self.set_parameter_value('df', df)
#            height= abs(y - self.pick_points[0].y)
#            if height != 0:
#                self.set_parameter_value('Z',self.calc_Z(height, self.parameters))
#        elif self.current_pick == 2:
#            dist = x - self.get_parameter_value('f_r')
#            if dist != 0:
#                phi = dist/self.parameters[self.prefix + "df"].value * np.pi/2/2
#                self.parameters[self.prefix + "phi"].value = phi

#class Pick_VNAFMR_IPFieldModel(VNAFMR_IPFieldModel, PickModel):
#    def __init__(self, *args, prefix=None, **kwargs):
#        PickModel.__init__(self, 3, prefix=prefix)
#        super().__init__(prefix=self._prefix, **kwargs)
#        self.parameters = self.make_params()
#        self.N = (0, 0, 1)
#
#    def update_current_parameter(self, x, y):
#        if self.current_pick == 0:
#            self.set_parameter_value('B_r', x)
#            self.set_parameter_value('Z', self.calc_Z(y, self.parameters))
#        elif self.current_pick == 1:
#            dB = abs(x - self.get_parameter_value('B_r'))
#            self.set_parameter_value('dB', dB)
#            height= abs(y - self.pick_points[0].y)
#            if height != 0:
#                self.set_parameter_value('Z',self.calc_Z(height, self.parameters))
#        elif self.current_pick == 2:
#            dist = x - self.get_parameter_value('B_r')
#            if dist != 0:
#                phi = dist/self.parameters[self.prefix + "dB"].value * np.pi/2/2
#                self.parameters[self.prefix + "phi"].value = phi

class Pick_FreqKittelIPModel(FreqKittelIPModel, PickModel):
    def __init__(self, *args, prefix=None, **kwargs):
        PickModel.__init__(self, 2, prefix=prefix)
        super().__init__(prefix=self._prefix, **kwargs)
        self.parameters = self.make_params()

    def update_current_parameter(self, x, y):
        raise NotImplementedError


class Pick_FreqKittelOOPModel(FreqKittelOOPModel, PickModel):
    def __init__(self, *args, prefix=None, **kwargs):
        PickModel.__init__(self, 2, prefix=prefix)
        super().__init__(prefix=self._prefix, **kwargs)
        self.parameters = self.make_params()

    def update_current_parameter(self, x, y):
        raise NotImplementedError



models_dict = OrderedDict()
models_dict["Gaussian Model"] = PickGaussianModel
models_dict["Lorentzian Model"] = LorentzianPickModel
models_dict["PseudoVoigt Model"] = PseudoVoigtPickModel
models_dict["Quadratic Model"] = PickQuadraticModel
models_dict["Linear Model"] = PickLinearModel
models_dict["Constant Model"] = PickConstantModel
models_dict["cLinear"] = Pick_ComplexLinearModel
models_dict["cVNAFMR_Sphere"] = Pick_VNAFMR_EllipsoidSphereModel
models_dict["cVNAFMR_DerivativeSphere"] = Pick_VNAFMR_EllipsoidDerivativeSphereModel
models_dict["cVNAFMR_DifferenceQuotientSphere"] = Pick_VNAFMR_EllipsoidDifferenceQuotientSphereModel
#models_dict["cVNAFMR_IPFieldModel"] = Pick_VNAFMR_IPFieldModel
#models_dict["cVNAFMR_DerivativeEllipsoidOOP"] = Pick_VNAFMR_EllipsoidDerivativeOOPQuickModel
models_dict["BBFMR_ResonanceFreq_Kittel_IP"] = Pick_FreqKittelIPModel
models_dict["BBFMR_ResonanceFreq_Kittel_OOP"] = Pick_FreqKittelOOPModel
