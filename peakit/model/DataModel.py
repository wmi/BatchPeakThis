# -*- coding: utf8 -*-
import numpy as np
from guidata.qt import QtCore
from lmfit import Parameters
from bbfmr.complex_model import ComplexCompositeModel, ComplexModelResult
import operator
import json
import jsonpickle
from importlib import import_module

from peakit.model.Spectrum import Spectrum
from peakit.model.BackgroundModel import BackgroundModel


class DataModel(QtCore.QObject):
    spectrum_changed = QtCore.pyqtSignal(Spectrum)

    background_changed = QtCore.pyqtSignal(Spectrum)
    background_points_changed = QtCore.pyqtSignal(Spectrum)

    models_changed = QtCore.pyqtSignal()
    model_deleted = QtCore.pyqtSignal(int)
    model_parameters_changed = QtCore.pyqtSignal(int, Spectrum)

    model_sum_changed = QtCore.pyqtSignal(Spectrum)

    residual_changed = QtCore.pyqtSignal(Spectrum)

    roi_value_changed = QtCore.pyqtSignal(float, float)

    fit_succeeded = QtCore.pyqtSignal()

    def __init__(self):
        super(DataModel, self).__init__()
        self.models = []
        self.models_sum = Spectrum()

        self.background_model = BackgroundModel()
        self.background_model.background_model_changed.connect(self.update_background)
        self._background_subtracted_flag = False

        self.spectrum = Spectrum()
        self.background_spectrum = Spectrum([], [])
        self.residual = Spectrum()
        self._roi = None
        self.fit = None

        self.update_background()

    ######################################
    # %% Region of interest
    @property
    def roi(self):
        return self._roi

    @roi.setter
    def roi(self, values):
        self._roi = values
        self.spectrum_changed.emit(self.get_spectrum())
        self.update_background()
        self.roi_value_changed.emit(*self.get_roi())

    def set_roi(self, x_min, x_max, emit=True):
        """ Set roi min/max in units of x """
        x, _ = self.get_whole_spectrum_data()
        roi = (np.where((x_min <= x) & (x <= x_max)))[0]
        self.roi = roi

    def get_roi(self):
        """ Get roi in units of x. Returns only one single roi """
        x, _ = self.get_whole_spectrum_data()

        if self.roi is None or len(self.roi) == 0:
            return (x[0], x[-1])
        else:
            return (x[self.roi[0]], x[self.roi[-1]])

    def recommended_roi(self, arg=None):
        x, y = self.get_whole_spectrum_data()
        roi = []
        for i, m in enumerate(self.models):
            try:
                roi = np.union1d(roi, m.recommended_roi(x, y, arg))
            except AttributeError:
                pass
        roi = np.array(roi, dtype=int)
        roi = np.array(roi[np.where(roi < len(x))], dtype=int)

        if len(roi) == 0:
            roi = np.arange(0, len(x))
        return np.sort(roi)

    ######################################
    # %% Data handling
    def load_data(self, filename):
        self.spectrum.load(filename)
        self.update_background()
        self.spectrum_changed.emit(self.get_spectrum())

    def set_spectrum_data(self, x, y, emit=True):
        self.spectrum.data = x, y
        self.update_background(emit=emit)
        if emit:
            self.spectrum_changed.emit(self.get_spectrum())

    def get_whole_spectrum(self):
        if self._background_subtracted_flag and len(self.background_spectrum):
            return self.spectrum - self.background_spectrum
        else:
            return self.spectrum

    def get_whole_spectrum_data(self):
        return self.get_whole_spectrum().data

    def get_spectrum(self):
        if self.roi is not None:
            return self.get_whole_spectrum().limit(self.roi)
        return self.get_whole_spectrum()

    def get_spectrum_data(self):
        return self.get_spectrum().data

    ######################################
    # %% Background picking
    def add_background_model_point(self, x, y):
        if self._background_subtracted_flag:
            y_bkg = self.background_model.data(x)
            self.background_model.add_point(x, y+y_bkg)
        else:
            self.background_model.add_point(x, y)
        self.background_points_changed.emit(self.get_background_points_spectrum())

    def remove_background_model_point_close_to(self, x, y):
        if self._background_subtracted_flag:
            y += self.background_model.data(x)
        self.background_model.delete_point_close_to(x, y)
        self.background_points_changed.emit(self.get_background_points_spectrum())

    def update_background(self, emit=True):
        # emit background model
        x, y = self.get_spectrum_data()
        bkg_y = self.background_model.data(x)
        if bkg_y is not None:
            self.background_spectrum = Spectrum(x, bkg_y)
        else:
            self.background_spectrum = Spectrum(np.array([]),
                                                np.array([], dtype=y.dtype))

        if emit:
            self.background_changed.emit(self.get_background_spectrum())

            self.model_sum_changed.emit(self.get_model_sum_spectrum())
            for ind in range(len(self.models)):
                self.model_parameters_changed.emit(ind, self.get_model_spectrum(ind))

            if self._background_subtracted_flag:
                self.spectrum_changed.emit(self.get_spectrum())

    def set_background_subtracted(self, bool):
        self._background_subtracted_flag = bool

        self.spectrum_changed.emit(self.get_spectrum())
        self.model_sum_changed.emit(self.get_model_sum_spectrum())

        self.background_changed.emit(self.get_background_spectrum())
        self.background_points_changed.emit(self.get_background_points_spectrum())

        for ind in range(len(self.models)):
            self.model_parameters_changed.emit(ind, self.get_model_spectrum(ind))

    def get_background_spectrum(self):
        if self._background_subtracted_flag:
            x, y = self.spectrum.data
            return Spectrum(x, np.zeros(y.shape, dtype=y.dtype))
        else:
            return self.background_spectrum

    def get_background_points_spectrum(self):
        if self._background_subtracted_flag:
            x, y = self.background_model.get_points()
            y -= self.background_model.data(x)
            return Spectrum(x, y)
        else:
            x, y = self.background_model.get_points()
            return Spectrum(x, y)

    ######################################
    # %% Models
    def add_model(self, model):
        self.models.append(model)
        self.models_changed.emit()

    def update_model(self, ind, parameters):
        for key, val in parameters.items():
            if not key.startswith(self.models[ind].prefix):
                model_key = self.models[ind].prefix + key
            else:
                model_key = key
            self.models[ind].parameters[model_key].value = parameters[key].value
            self.models[ind].parameters[model_key].vary = parameters[key].vary
            self.models[ind].parameters[model_key].min = parameters[key].min
            self.models[ind].parameters[model_key].max = parameters[key].max

        self.model_parameters_changed.emit(ind, self.get_model_spectrum(ind))
        self.model_sum_changed.emit(self.get_model_sum_spectrum())

    def recall_models(self, fit):
        self.models = fit.model.components
        self.models_changed.emit()

        for i, model in enumerate(self.models):
            for key, val in fit.best_values.items():
                if key in model.parameters.keys():
                    model.parameters[key].value = val
            self.model_parameters_changed.emit(i, self.get_model_spectrum(i))
        x, y = self.get_spectrum_data()

        self.model_sum_changed.emit(self.get_model_sum_spectrum())
        self.residual_changed.emit(Spectrum(x, fit.eval(x=x) - y))

    def get_model_spectrum(self, ind):
        x, _ = self.get_spectrum_data()

        y = self.models[ind].quick_eval(x)

        if not self._background_subtracted_flag:
            _, y_bkg = self.background_spectrum.data
            if x.shape == y_bkg.shape:
                y += y_bkg

        return Spectrum(x, y)

    def get_model_sum_spectrum(self):
        x, y = self.get_spectrum_data()
        sum_spectrum = np.zeros(x.shape, dtype=y.dtype)

        if not self._background_subtracted_flag:
            _, y_bkg = self.background_spectrum.data
            if x.shape == y_bkg.shape:
                sum_spectrum += y_bkg

        for m in self.models:
            sum_spectrum += m.quick_eval(x)

        return Spectrum(x, sum_spectrum)

    def update_current_model_parameter(self, ind, x, y):
        y -= self.background_model.data(x)

        self.models[ind].update_current_parameter(x, y)
        self.model_parameters_changed.emit(ind, self.get_model_spectrum(ind))
        self.model_sum_changed.emit(self.get_model_sum_spectrum())

    def pick_current_model_parameters(self, ind, x, y):
        y -= self.background_model.data(x)

        more_parameters_available = self.models[ind].pick_parameter(x, y)
        self.model_parameters_changed.emit(ind, self.get_model_spectrum(ind))
        self.model_sum_changed.emit(self.get_model_sum_spectrum())
        return more_parameters_available

    def del_model(self, index=-1):
        del self.models[index]
        self.model_sum_changed.emit(self.get_model_sum_spectrum())
        self.model_deleted.emit(index)

    def combine_models(self):
        if len(self.models) == 0:
            return None, None
        elif len(self.models) == 1:
            return self.models[0], self.models[0].parameters

        c_model = ComplexCompositeModel(self.models[0], self.models[1],
                                        operator.add)
        c_parameters = self.models[0].parameters + self.models[1].parameters
        for m in self.models[2:]:
            c_model += m
            c_parameters += m.parameters

        return c_model, c_parameters

    ######################################
    # %% Fitting
    def initialize_fit(self, guess_value_only=True, **kwargs):
        """
        Initialize (guess starting values) the fit. If guess_value_only is True,
        the current parameter attributes are kept (vary, min, max) otherwise
        they are reset to the model defaults.
        """
        x, y = self.get_spectrum_data()
        x_bkg, y_bkg = self.background_spectrum.data
        if x.shape == y_bkg.shape:
            y -= y_bkg

        for i, m in enumerate(self.models):
            parameters_vary = {key: m.parameters[key].vary for key in m.parameters}            
            parameters_min = {key: m.parameters[key].min for key in m.parameters}            
            parameters_max = {key: m.parameters[key].max for key in m.parameters}            
            m.parameters = m.guess(data=y, x=x, **kwargs)
            if guess_value_only:
                for key in m.parameters:
                    m.parameters[key].vary = parameters_vary[key] 
                    m.parameters[key].min = parameters_min[key] 
                    m.parameters[key].max = parameters_max[key] 
            y -= m.eval(params=m.parameters, x=x)
            self.model_parameters_changed.emit(i, self.get_model_spectrum(i))

        self.model_sum_changed.emit(self.get_model_sum_spectrum())

    def fit_data(self):
        if len(self.models) == 0:
            return

        combined_model, combined_parameters = self.combine_models()

        x, y = self.get_spectrum_data()
        x_bkg, y_bkg = self.background_spectrum.data
        if x.shape == y_bkg.shape:
            y -= y_bkg

        self.fit = combined_model.fit(y, params=combined_parameters, x=x)

        # save the data into the model
        for i, model in enumerate(self.models):
            for key, val in self.fit.best_values.items():
                if key in model.parameters.keys():
                    model.parameters[key].value = val
            self.model_parameters_changed.emit(i, self.get_model_spectrum(i))

        self.model_sum_changed.emit(self.get_model_sum_spectrum())
        self.residual_changed.emit(Spectrum(x, self.fit.eval(x=x) - y))
        self.fit_succeeded.emit()

        return self.fit

    ######################################
    # %% Saving/Loading
    def save_data(self, filename):
        data_x, data_y = self.get_spectrum_data()
        _, model_sum = self.get_model_sum_spectrum().data
        residual = data_y-model_sum
        _, bkg = self.get_background_spectrum().data
        out_data = np.vstack((data_x, data_y, model_sum, residual, bkg))

        header = 'x, y, model_sum, res, bkg'
        for ind in range(len(self.models)):
            _, model = self.get_model_spectrum(ind).data
            out_data = np.vstack((out_data, model))
            header += ', model {}'.format(ind)
        np.savetxt(filename, out_data.T, header=header)

    def get_fit_model_classes(self):
        classes = []
        for m in self.fit.model.components:
            classes.append(m.__class__.__module__ + '.' + m.__class__.__name__)

        return classes

    def models_loads(self, model_classes, prefixes, params=None):
        models = []
        for i, m_class in enumerate(model_classes):
            m = self.model_loads(m_class, prefixes[i], all_params=params)
            models.append(m)

        return models

    def model_loads(self, models_class, prefix, all_params=None):
        package, class_name = models_class.rsplit('.', 1)
        module = import_module(package)
        cls = getattr(module, class_name)

        m = cls(prefix=prefix)

        if all_params is not None:
            params = Parameters()
            for k, p in all_params.items():
                if k.startswith(prefix):
                    params[k] = p
            m.parameters = params

        return m

    def load_fit(self, filename):
        with open(filename) as f:
            data = json.load(f)
        
            params = Parameters()
            for p in data["params"]:
                if p == "py/object":
                    continue
                params.add(jsonpickle.loads(json.dumps(data["params"][p]))) # FIXME: IHHH
            
            model_class_names = data["models"]
            prefixes = data["prefixes"]
            models = self.models_loads(model_class_names, prefixes,
                                       params=params)
            self.models = models
            combined_model, _ = self.combine_models()

            self.fit = ComplexModelResult(combined_model, params=params)
            self.fit.best_values = self.fit.model._make_all_args(params)
            return self.fit

    def save_models(self, filename):
        pass

