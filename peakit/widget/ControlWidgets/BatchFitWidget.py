# -*- coding: utf-8 -*-
from guidata.qt import QtGui

from peakit.widget.CustomWidgets.GuiElements import FlatButton


class BatchFitWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(BatchFitWidget, self).__init__(parent)
        self.main_layout = QtGui.QHBoxLayout()
        self.main_layout.setContentsMargins(0, 0, 0, 0)
        self.main_layout.setSpacing(5)

        self.fit_btn = FlatButton('Fit all')
        self.main_layout.addWidget(self.fit_btn)

        self.setLayout(self.main_layout)
