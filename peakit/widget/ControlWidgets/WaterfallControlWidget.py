# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 16:32:31 2016

@author: hannes.maierflaig
"""

from guidata.qt import QtGui, QtCore


class WaterfallControlWidget(QtGui.QWidget):
    slider_steps = 100.

    def __init__(self, parent=None):
        super(WaterfallControlWidget, self).__init__(parent)

        self.grid_layout = QtGui.QGridLayout()
        self.grid_layout.setContentsMargins(0, 0, 0, 0)
        self.grid_layout.setSpacing(5)

        self.shift_text = QtGui.QLabel("Shift x-axis w/ slope:")
        self.shift_edit = QtGui.QLineEdit("28.024e9")
        self.splitting_text = QtGui.QLabel("Offset between slices:")
        self.splitting_slider = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.splitting_slider.setRange(0, self.slider_steps)
        self.splitting_slider.setValue(50)
        self.splitting_label = QtGui.QLabel("%.2f" % 0.5)

        self.grid_layout.addWidget(self.splitting_text, 0, 0)
        self.grid_layout.addWidget(self.splitting_slider, 1, 0)
        self.grid_layout.addWidget(self.splitting_label, 1, 1)
        self.grid_layout.addWidget(self.shift_text, 2, 0)
        self.grid_layout.addWidget(self.shift_edit, 3, 0)

        self.setLayout(self.grid_layout)

        self.splitting_slider.valueChanged.connect(self.slider_valueChange)


    def slider_valueChange(self):
        self.splitting_label.setText("%.2f" % (self.splitting_slider.value() /
                                               self.slider_steps))
