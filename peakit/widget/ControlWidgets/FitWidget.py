# -*- coding: utf8 -*-
from guidata.qt import QtGui

from peakit.widget.CustomWidgets.GuiElements import FlatButton


class FitWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(FitWidget, self).__init__(parent)
        self.main_layout = QtGui.QHBoxLayout()
        self.main_layout.setContentsMargins(0, 0, 0, 0)
        self.main_layout.setSpacing(5)

        self.initialize_btn= FlatButton('Initialize')
        self.fit_btn = FlatButton('Fit')
        self.main_layout.addWidget(self.initialize_btn)
        self.main_layout.addWidget(self.fit_btn)

        self.setLayout(self.main_layout)
