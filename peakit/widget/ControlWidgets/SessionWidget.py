# -*- coding: utf8 -*-
from guidata.qt import QtGui

from peakit.widget.CustomWidgets.GuiElements import FlatButton


class SessionWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(SessionWidget, self).__init__(parent)

        self.grid_layout = QtGui.QGridLayout()
        self.grid_layout.setContentsMargins(0, 0, 0, 0)
        self.grid_layout.setSpacing(5)

        self.load_measurement_btn = FlatButton("Load Measurement")
        self.save_measurement_btn = FlatButton("Save Measurement")

        self.load_session_btn = FlatButton("Load Session")
        self.save_session_btn = FlatButton("Save Session")

        self.grid_layout.addWidget(self.load_measurement_btn, 0, 0)
        self.grid_layout.addWidget(self.save_measurement_btn, 0, 1)
        self.grid_layout.addWidget(self.load_session_btn, 2, 0)
        self.grid_layout.addWidget(self.save_session_btn, 2, 1)

        self.setLayout(self.grid_layout)
