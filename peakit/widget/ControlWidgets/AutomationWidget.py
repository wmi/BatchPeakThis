# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 16:32:31 2016

@author: hannes.maierflaig
"""

from guidata.qt import QtGui, QtCore

from peakit.widget.CustomWidgets.GuiElements import FlatButton
from peakit.widget.ControlWidgets.ModelWidget import ModelSelectorDialog

class AutomationWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(AutomationWidget, self).__init__(parent)

        self.main_layout = QtGui.QVBoxLayout()
        self.initialize_checkbox = QtGui.QCheckBox("Initialize (guess)")
        self.peakdetect_checkbox = QtGui.QCheckBox("Peakdetect")
        self.param_checkbox = QtGui.QCheckBox("Set param %s to current dataset value" % "x")
        self.auto_roi_checkbox = QtGui.QCheckBox("Automatically determine roi. Arg:")
        self.auto_roi_arg = QtGui.QLineEdit("1")
        self.auto_roi_arg.setFixedWidth(30)

        self.auto_roi = QtGui.QHBoxLayout()
        self.auto_roi.addWidget(self.auto_roi_checkbox)
        self.auto_roi.addWidget(self.auto_roi_arg)

        self.main_layout.addLayout(self.auto_roi)
        self.main_layout.addWidget(self.initialize_checkbox)
        self.main_layout.addWidget(self.peakdetect_checkbox)
        self.main_layout.addWidget(self.param_checkbox)
        #self.main_layout.addWidget(self.peakfind_holdoff_label)
        #self.main_layout.addWidget(self.peakfind_holdoff)
        #self.main_layout.addWidget(self.peakfind_threshold_label)
        #self.main_layout.addWidget(self.peakfind_threshold)
        #self.main_layout.addWidget(self.peakfind_checkbox)
        #self.main_layout.addWidget(self.select_model_btn)

        #self.model_selector_dialog = ModelSelectorDialog(self)

        self.setLayout(self.main_layout)

    def show_model_selector_dialog(self):
        self.model_selector_dialog.show()