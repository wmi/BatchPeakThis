# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 13:51:02 2016

@author: hannes.maierflaig
"""

from guidata.qt import QtGui, QtCore

from peakit.widget.CustomWidgets.GuiElements import FlatButton
from peakit.widget import WaterfallDialog

class DataNavWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(DataNavWidget, self).__init__(parent)

        self.grid_layout = QtGui.QGridLayout()
        self.grid_layout.setContentsMargins(0,0,0,0)
        self.grid_layout.setSpacing(5)

        self.waterfall_btn = FlatButton("Waterfall plot")
        self.prev_btn = FlatButton("Previous Set")
        self.reset_btn = FlatButton("Delete this fit")
        self.next_btn = FlatButton("Next Set")

        self.datasets_slider = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.datasets_slider.setRange(0, 10)
        self.current_spin = QtGui.QSpinBox()
        self.current_spin.setRange(0, 10)
        self.current_label = QtGui.QLabel("of %d"%10)


        self.grid_layout.addWidget(self.datasets_slider, 0, 0)
        self.grid_layout.addWidget(self.current_spin, 0, 1)
        self.grid_layout.addWidget(self.current_label, 0, 2)

        self.grid_layout.addWidget(self.prev_btn, 1, 0)
        self.grid_layout.addWidget(self.reset_btn, 1, 1)
        self.grid_layout.addWidget(self.next_btn, 1, 2)

        self.grid_layout.addWidget(self.waterfall_btn, 2, 0, 1, 3)

        self.setLayout(self.grid_layout)

        self.datasets_slider.valueChanged.connect(self.slider_valueChange)
        self.current_spin.valueChanged.connect(self.spin_valueChange)

        self.waterfall_dialog = WaterfallDialog.WaterfallDialog(self)

    def slider_valueChange(self):
        self.current_spin.setValue(self.datasets_slider.value())

    def spin_valueChange(self):
        self.datasets_slider.setValue(self.current_spin.value())
