# -*- coding: utf-8 -*-
from guidata.qt import QtGui

from peakit.widget.CustomWidgets.GuiElements import FlatButton
import pyqtgraph as pg

class VisualizeFitWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(VisualizeFitWidget, self).__init__(parent)

        self.grid_layout = QtGui.QGridLayout()
        self.grid_layout.setContentsMargins(0, 0, 0, 0)
        self.grid_layout.setSpacing(5)

        self.visualize_x = QtGui.QComboBox()
        self.visualize_y = QtGui.QComboBox()
        self.delete_fits_btn = FlatButton("Del visible fits")
        self.visualize_btn = FlatButton("Visualize fit parameter")

        self.grid_layout.addWidget(self.create_plot(), 0, 0, 1, 2)
        self.grid_layout.addWidget(self.visualize_x, 1, 0)
        self.grid_layout.addWidget(self.visualize_y, 1, 1)
        self.grid_layout.addWidget(self.delete_fits_btn, 2, 0)
        self.grid_layout.addWidget(self.visualize_btn, 2, 1)

        self.setLayout(self.grid_layout)

    def create_plot(self):
        self._pg_layout_widget = pg.GraphicsLayoutWidget()
        self._pg_layout_widget.setBackground('#3C3C3C')

        self._pg_layout = pg.GraphicsLayout()
        self._pg_layout.setContentsMargins(0, 0, 0, 0)
        self._pg_layout_widget.setContentsMargins(0, 0, 0, 0)

        self._plot_item = pg.PlotItem()
        self._pg_layout.addItem(self._plot_item, 0, 0)
        self._pg_layout_widget.addItem(self._pg_layout)
        self._data_item = pg.ScatterPlotItem(pen=pg.mkPen('#ccc', width=1),
                                             brush=pg.mkBrush('#ccc'),
                                             size=5, symbol='x')
        self._current_item = pg.ScatterPlotItem(pen=pg.mkPen('#d44', width=1),
                                                brush=pg.mkBrush('#d44'),
                                                 size=5, symbol='x')

        self._plot_item.axes["left"]["item"].setPen(pg.mkPen("#ddd"))
        self._plot_item.axes["bottom"]["item"].setPen(pg.mkPen("#ddd"))
        self._plot_item.addItem(self._data_item)
        self._plot_item.addItem(self._current_item)

        return self._pg_layout_widget

    def plot(self, x, y, labels=("", "")):
        self._data_item.clear()
        self._data_item.setData(x, y)
        self._plot_item.setLabel("bottom", labels[0])
        self._plot_item.setLabel("left", labels[1])
        self._plot_item.enableAutoRange()

    def plot_current(self, x, y):
        self._current_item.clear()
        self._current_item.setData(x, y)