# -*- coding: utf8 -*-
from peakit.widget.ControlWidgets.BackgroundWidget import BackgroundWidget
from peakit.widget.ControlWidgets.FileWidget import FileWidget
from peakit.widget.ControlWidgets.FitWidget import FitWidget
from peakit.widget.ControlWidgets.ModelWidget import ModelWidget
from peakit.widget.ControlWidgets.DataNavWidget import DataNavWidget
from peakit.widget.ControlWidgets.AutomationWidget import AutomationWidget
from peakit.widget.ControlWidgets.ModelWidget import ModelSelectorDialog
from peakit.widget.ControlWidgets.RoiWidget import RoiWidget
from peakit.widget.ControlWidgets.BatchFitWidget import BatchFitWidget
from peakit.widget.ControlWidgets.WaterfallControlWidget import WaterfallControlWidget
from peakit.widget.ControlWidgets.PeakdetectWidget import PeakdetectWidget
from peakit.widget.ControlWidgets.VisualizeFitWidget import VisualizeFitWidget
from peakit.widget.ControlWidgets.SessionWidget import SessionWidget
