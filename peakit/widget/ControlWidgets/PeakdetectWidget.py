# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 16:32:31 2016

@author: hannes.maierflaig
"""

from guidata.qt import QtGui, QtCore
from ..CustomWidgets.GuiElements import FlatButton


class PeakdetectWidget(QtGui.QWidget):
    slider_steps = 100.

    def __init__(self, parent=None):
        super(PeakdetectWidget, self).__init__(parent)

        self.grid_layout = QtGui.QGridLayout()
        self.grid_layout.setContentsMargins(0, 0, 0, 0)
        self.grid_layout.setSpacing(5)

        self.delta_text = QtGui.QLabel("Delta (y-unit)")
        self.delta_edit = QtGui.QLineEdit("0.01")
        self.lookahead_text = QtGui.QLabel("Lookahead (points)")
        self.lookahead_edit = QtGui.QLineEdit("100")
        self.peakdetect_btn = FlatButton("Detect peaks")
        self.minmax_combo = QtGui.QComboBox()
        self.minmax_combo.addItems(["Maxima", "Minima"])
        
        self.grid_layout.addWidget(self.delta_text, 0, 0)
        self.grid_layout.addWidget(self.delta_edit, 1, 0)
        self.grid_layout.addWidget(self.lookahead_text, 0, 1)
        self.grid_layout.addWidget(self.lookahead_edit, 1, 1)
        self.grid_layout.addWidget(self.minmax_combo, 2, 0, 1, 2)
        self.grid_layout.addWidget(self.peakdetect_btn, 3, 0, 1, 2)

        self.setLayout(self.grid_layout)