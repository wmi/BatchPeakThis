# -*- coding: utf-8 -*-
"""
Created on Mon Apr 18 16:45:11 2016

@author: hannes.maierflaig
"""

from guidata.qt import QtGui, QtCore
from ..CustomWidgets.GuiElements import FlatButton


class RoiWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(RoiWidget, self).__init__(parent)

        self.grid_layout = QtGui.QGridLayout()
        self.grid_layout.setContentsMargins(0, 0, 0, 0)
        self.grid_layout.setSpacing(5)

        self.reset_last_roi_btn = FlatButton("Undo last ROI")
        self.reset_roi_btn = FlatButton("Reset ROIs")
        self.apply_roi_btn = FlatButton("Set ROI")
        self.roi_width_label = QtGui.QLabel("ROI width:")
        self.roi_width = QtGui.QLineEdit("1")

        self.grid_layout.addWidget(self.roi_width_label, 0, 0)
        self.grid_layout.addWidget(self.roi_width, 0, 1)
        self.grid_layout.addWidget(self.reset_roi_btn, 1, 0, 1, 2)
        self.grid_layout.addWidget(self.apply_roi_btn, 2, 0)
        self.grid_layout.addWidget(self.reset_last_roi_btn, 2, 1)

        self.setLayout(self.grid_layout)
