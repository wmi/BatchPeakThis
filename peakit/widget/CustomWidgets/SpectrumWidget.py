# -*- coding: utf8 -*-
from guidata.qt import QtGui, QtCore
import pyqtgraph as pg
import numpy as np

pg.setConfigOption('useOpenGL', False)
pg.setConfigOption('leftButtonPan', False)
pg.setConfigOption('background', 'w')
pg.setConfigOption('foreground', 'k')
pg.setConfigOption('antialias', True)

plot_colors = {
    'data_pen': '#CCC',
    'data_brush': '#CCC',
    'data_roi_pen': '#444',
    'data_roi_brush': '#444',
    'background_plot': 'r',
    'background_scatter_pen': 'r',
    'background_scatter_brush': 'k',
    'peak_scatter_pen': 'r',
    'peak_scatter_brush': 'r',
    'residual_pen': 'r',
    'residual_brush': '#DDD',
    'model': '#AA6633',
    'model-active': '#FFAA77',
    'model-sum': '#22FF77',
}


class SpectrumWidget(QtGui.QWidget):
    mouse_moved = QtCore.pyqtSignal(float, float)
    mouse_left_clicked = QtCore.pyqtSignal(float, float)
    range_changed = QtCore.pyqtSignal(list)

    def __init__(self, parent=None):
        super(SpectrumWidget, self).__init__(parent)

        self._layout = QtGui.QVBoxLayout()
        self._layout.setContentsMargins(0, 0, 0, 0)

        self._create_spectrum()
        self._create_mouse_position_widget()

        self.setLayout(self._layout)

        self._create_plot_items()

        self._spectrum_plot.connect_mouse_move_event()
        self._residual_plot.connect_mouse_move_event()

        self._spectrum_plot.mouse_moved.connect(self.update_mouse_position_widget)
        self._residual_plot.mouse_moved.connect(self.update_mouse_position_widget)

        self._spectrum_plot.mouse_left_clicked.connect(self.mouse_left_clicked.emit)
        self._data_plot_item.sigClicked.connect(self.data_plot_item_clicked)

        self._spectrum_plot.mouse_moved.connect(self.mouse_moved.emit)

        ######################
        # %% Imaginary display
        self._spectrum_plot_im.connect_mouse_move_event()
        self._residual_plot_im.connect_mouse_move_event()

        self._spectrum_plot_im.mouse_moved.connect(self.update_mouse_position_widget)
        self._residual_plot_im.mouse_moved.connect(self.update_mouse_position_widget)

        self._spectrum_plot_im.mouse_left_clicked.connect(self.mouse_left_clicked.emit)
        self._data_plot_item_im.sigClicked.connect(self.data_plot_item_clicked)

        self._spectrum_plot.mouse_moved.connect(self.mouse_moved.emit)

    def _create_spectrum(self):
        self._pg_layout_widget = pg.GraphicsLayoutWidget()
        self._pg_layout = pg.GraphicsLayout()
        self._pg_layout.setContentsMargins(0, 0, 0, 0)
        self._pg_layout_widget.setContentsMargins(0, 0, 0, 0)

        self._spectrum_plot = ModifiedPlotItem()
        self._residual_plot = ModifiedPlotItem()

        self._pg_layout.addItem(self._spectrum_plot, 0, 0)
        self._pg_layout.addItem(self._residual_plot, 1, 0)

        self._residual_plot.setXLink(self._spectrum_plot)
        self._residual_plot.hideAxis('bottom')

        ######################
        # %% Imaginary display
        self._spectrum_plot_im = ModifiedPlotItem()
        self._residual_plot_im = ModifiedPlotItem()

        self._pg_layout.addItem(self._spectrum_plot_im, 0, 1)
        self._pg_layout.addItem(self._residual_plot_im, 1, 1)

        self._spectrum_plot.setXLink(self._spectrum_plot_im)
        self._residual_plot_im.setXLink(self._spectrum_plot_im)
        self._residual_plot_im.hideAxis('bottom')

        self._pg_layout.layout.setRowStretchFactor(0, 10)
        self._pg_layout.layout.setRowStretchFactor(1, 2)

        self._pg_layout_widget.addItem(self._pg_layout)
        self._layout.addWidget(self._pg_layout_widget)

    def _create_mouse_position_widget(self):
        self._pos_layout = QtGui.QHBoxLayout()
        self.x_lbl = QtGui.QLabel('x:')
        self.y_lbl = QtGui.QLabel('y:')

        self.x_lbl.setMinimumWidth(60)
        self.y_lbl.setMinimumWidth(60)

        self._pos_layout.addSpacerItem(QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.Expanding,
                                                         QtGui.QSizePolicy.Fixed))
        self._pos_layout.addWidget(self.x_lbl)
        self._pos_layout.addWidget(self.y_lbl)

        self._layout.addLayout(self._pos_layout)

    def data_plot_item_clicked(self):
        """
        this function is called when the data plot item is clicked and just emits the mouse_left_clicked signal
        x, y values to
        mouse_left_clicked. Otherwise the Data plot item blocks the signal...
        """
        x, y = self._spectrum_plot.get_mouse_position()
        self.mouse_left_clicked.emit(x, y)

    def update_mouse_position_widget(self, x, y):
        self.x_lbl.setText('x: {:02.4f}'.format(x))
        self.y_lbl.setText('y: {:02.4f}'.format(y))

    def _create_plot_items(self):
        self.current_selected_model = 0
        self._data_plot_item = ModifiedScatterPlotItem(pen=pg.mkPen(plot_colors['data_pen'], width=1),
                                                  brush=pg.mkBrush(plot_colors['data_brush']),
                                                  size=5,
                                                  symbol='x',
                                                  downsample=5,
                                                  clipToView=True)

        self._data_roi_plot_item = ModifiedScatterPlotItem(pen=pg.mkPen(plot_colors['data_roi_pen'], width=1),
                                                      brush=pg.mkBrush(plot_colors['data_roi_brush']),
                                                      size=5,
                                                      symbol='x',
                                                      downsample=5,
                                                      clipToView=True)

        self._background_plot_item = pg.PlotDataItem(pen=pg.mkPen(plot_colors['background_plot'],
                                                                  width=1.5))

        self._background_scatter_item = pg.ScatterPlotItem(pen=pg.mkPen(plot_colors['background_scatter_pen'], width=1),
                                                           brush=pg.mkBrush(plot_colors['background_scatter_brush']),
                                                           size=8,
                                                           symbol='d')

        self._residual_plot_item = pg.ScatterPlotItem(pen=pg.mkPen(plot_colors['residual_pen'],
                                                                   width=1),
                                                      brush=pg.mkBrush(plot_colors['residual_brush']),
                                                      size=2,
                                                      symbol='o')

        self._peak_plot_item = pg.ScatterPlotItem(pen=pg.mkPen(plot_colors['peak_scatter_pen'], width=1),
                                                           brush=pg.mkBrush(plot_colors['peak_scatter_brush']),
                                                           size=8,
                                                           symbol='d')

        self._model_sum_plot_item = pg.PlotDataItem(pen=pg.mkPen(plot_colors['model-sum'],
                                                                 width=2.5))

        self._model_plot_items = []

        self._spectrum_plot.addItem(self._data_plot_item)
        self._spectrum_plot.addItem(self._data_roi_plot_item)
        self._spectrum_plot.addItem(self._background_plot_item)
        self._spectrum_plot.addItem(self._model_sum_plot_item)
        self._spectrum_plot.addItem(self._background_scatter_item)
        self._spectrum_plot.addItem(self._peak_plot_item)
        self._residual_plot.addItem(self._residual_plot_item)
        

        self.linear_region_item = ModifiedLinearRegionItem([5, 20], pg.LinearRegionItem.Vertical, movable=False)
        self._spectrum_plot.addItem(self.linear_region_item)

        ######################
        # %% Imaginary display
        self._data_plot_item_im = ModifiedScatterPlotItem(pen=pg.mkPen(plot_colors['data_pen'], width=1),
                                                  brush=pg.mkBrush(plot_colors['data_brush']),
                                                  size=5,
                                                  symbol='x',
                                                  downsample=5,
                                                  clipToView=True)

        self._data_roi_plot_item_im = ModifiedScatterPlotItem(pen=pg.mkPen(plot_colors['data_roi_pen'], width=1),
                                                      brush=pg.mkBrush(plot_colors['data_roi_brush']),
                                                      size=5,
                                                      symbol='x',
                                                      downsample=5,
                                                      clipToView=True)

        self._background_plot_item_im = pg.PlotDataItem(pen=pg.mkPen(plot_colors['background_plot'],
                                                                  width=1.5))

        self._background_scatter_item_im = pg.ScatterPlotItem(pen=pg.mkPen(plot_colors['background_scatter_pen'], width=1),
                                                           brush=pg.mkBrush(plot_colors['background_scatter_brush']),
                                                           size=8,
                                                           symbol='d')

        self._residual_plot_item_im = pg.ScatterPlotItem(pen=pg.mkPen(plot_colors['residual_pen'],
                                                                   width=1),
                                                      brush=pg.mkBrush(plot_colors['residual_brush']),
                                                      size=2,
                                                      symbol='o')

        self._peak_plot_item_im = pg.ScatterPlotItem(pen=pg.mkPen(plot_colors['peak_scatter_pen'], width=1),
                                                           brush=pg.mkBrush(plot_colors['peak_scatter_brush']),
                                                           size=8,
                                                           symbol='d')
                                                  
        self._model_sum_plot_item_im = pg.PlotDataItem(pen=pg.mkPen(plot_colors['model-sum'],
                                                                 width=2.5))

        self._model_plot_items_im = []

        self._spectrum_plot_im.addItem(self._data_plot_item_im)
        self._spectrum_plot_im.addItem(self._data_roi_plot_item_im)
        self._spectrum_plot_im.addItem(self._background_plot_item_im)
        self._spectrum_plot_im.addItem(self._model_sum_plot_item_im)
        self._spectrum_plot_im.addItem(self._background_scatter_item_im)
        self._spectrum_plot_im.addItem(self._peak_plot_item_im)
        self._residual_plot_im.addItem(self._residual_plot_item_im)

    def set_labels(self, xlabel, ylabel):
        self._spectrum_plot.setLabel("bottom", xlabel)
        self._spectrum_plot.setLabel("left", ylabel)
        self._spectrum_plot_im.setLabel("bottom", xlabel)
        self._spectrum_plot_im.setLabel("right", "Im(%s)" % ylabel)

    def plot_data(self, x, y):
        if y.dtype != complex:
            self._spectrum_plot_im.hide()
            self._residual_plot_im.hide()
        else:
            self._spectrum_plot_im.show()
            self._residual_plot_im.show()
            
        self._data_plot_item.setData(x, np.real(y))
        self._data_plot_item_im.setData(x, np.imag(y))
        self.set_limits(x, y)

    def set_limits(self, x, y, spacing=0.25):
        if len(x) == 0:
            return
        y_compl = y
        y = np.real(y_compl)
        x_min = np.min(x)
        x_max = np.max(x)
        y_min = np.min(np.squeeze(y))
        y_max = np.max(np.squeeze(y))
        x_range = x_max - x_min
        x_space = spacing * x_range
        y_range = y_max - y_min
        y_space = spacing * y_range
        self._spectrum_plot.setLimits(xMin=x_min - x_space,
                                      xMax=x_max + x_space,
                                      yMin=y_min - y_space,
                                      yMax=y_max + y_space)
        y = np.imag(y_compl)
        y_min = np.min(np.squeeze(y))
        y_max = np.max(np.squeeze(y))
        y_range = y_max - y_min
        y_space = spacing * y_range
        self._spectrum_plot_im.setLimits(xMin=x_min - x_space,
                                         xMax=x_max + x_space,
                                         yMin=y_min - y_space,
                                         yMax=y_max + y_space)
        self._spectrum_plot.vb.autoRange()
        self._spectrum_plot.vb.enableAutoRange()
        self._spectrum_plot._auto_range = True

    def plot_data_spectrum(self, spectrum):
        self.plot_data(*spectrum.data)

    def plot_roi_data(self, x, y):
        self._data_roi_plot_item.setData(x, np.real(y))
        self._data_roi_plot_item_im.setData(x, np.imag(y))

    def plot_roi_spectrum(self, spectrum):
        self.plot_roi_data(*spectrum.data)

    def plot_background(self, x, y):
        self._background_plot_item.setData(x, y)
        self._background_plot_item_im.setData(x, y)

    def plot_background_spectrum(self, spectrum):
        x, y = spectrum.data
        self.plot_background(x, y)

    def plot_background_points(self, x, y):
        self._background_scatter_item.setData(x, np.real(y))
        self._background_scatter_item_im.setData(x, np.imag(y))

    def plot_background_points_spectrum(self, spectrum):
        x, y = spectrum.data
        self.plot_background_points(x, y)

    def get_background_plot_data(self):
        return self._background_plot_item.getData()

    def get_background_points_data(self):
        return self._background_scatter_item.getData()

    def plot_residual(self, x, y):
        self._residual_plot_item.setData(x, np.real(y))
        self._residual_plot_item_im.setData(x, np.imag(y))

    def plot_residual_spectrum(self, spectrum):
        x, y = spectrum.data
        self.plot_residual(x, y)

    def get_residual_plot_data(self):
        return self._residual_plot_item.getData()

    def plot_peak_points(self, x, y):
        self._peak_plot_item.setData(x, np.real(y))
        self._peak_plot_item_im.setData(x, np.imag(y))
        
    def plot_model_sum(self, x, y):
        self._model_sum_plot_item.setData(x, np.real(y))
        self._model_sum_plot_item_im.setData(x, np.imag(y))

    def plot_model_sum_spectrum(self, spectrum):
        x, y = spectrum.data
        self.plot_model_sum(x, y)

    def get_model_sum_plot_data(self):
        return self._model_sum_plot_item.getData()

    def add_model(self, spectrum=None):
        style_opts = {"pen": pg.mkPen(plot_colors['model'], width=1.5)}
        self._model_plot_items.append(pg.PlotDataItem(**style_opts))
        self._spectrum_plot.addItem(self._model_plot_items[-1])
        self._model_plot_items_im.append(pg.PlotDataItem(**style_opts))
        self._spectrum_plot_im.addItem(self._model_plot_items_im[-1])
        if spectrum is not None:
            self.update_model_spectrum(-1, spectrum)

    def update_model(self, ind, x, y):
        self._model_plot_items[ind].setData(x, np.real(y))
        self._model_plot_items_im[ind].setData(x, np.imag(y))

    def update_model_spectrum(self, ind, spectrum):
        x, y = spectrum.data
        self.update_model(ind, x, y)

    def activate_model_spectrum(self, ind):
        pen = pg.mkPen(plot_colors['model'], width=1.5)
        pen_active = pg.mkPen(plot_colors['model-active'], width=2)

        if len(self._model_plot_items):
            self._model_plot_items[self.current_selected_model].setPen(pen)
            self._model_plot_items[ind].setPen(pen_active)
            self._model_plot_items_im[self.current_selected_model].setPen(pen)
            self._model_plot_items_im[ind].setPen(pen_active)
            self.current_selected_model = ind

    def del_model(self, index=-1):
        self._spectrum_plot.removeItem(self._model_plot_items[index])
        del self._model_plot_items[index]

    def clear_models(self):
        for i in self._model_plot_items:
            self._spectrum_plot.removeItem(i)
        for i in self._model_plot_items_im:
            self._spectrum_plot_im.removeItem(i)
        self._model_plot_items = []
        self._model_plot_items_im = []

    def get_model_plot_data(self, index):
        return (self._model_plot_items[index].getData() +
                1j*self._model_plot_items_im[index].getData())


    def get_number_of_model_plots(self):
        return len(self._model_plot_items)

    def get_mouse_position(self):
        return self._spectrum_plot.get_mouse_position()

    def set_spectrum_plot_keypress_callback(self, fcn_callback):
        self._spectrum_plot.keyPressEvent = fcn_callback

    def set_spectrum_plot_focus(self):
        self._spectrum_plot.setFocus()

    # %% Function for testing the GUI:
    def _spectrum_plot_emit_mouse_click_event(self, x, y):
        self._spectrum_plot.mouse_left_clicked.emit(x, y)

    def _spectrum_plot_set_current_mouse_position(self, x, y):
        self._spectrum_plot.update_cur_mouse_position(x, y)

    def _spectrum_plot_send_keypress_event(self, evt):
        self._spectrum_plot.keyPressEvent(evt)


class ModifiedPlotItem(pg.PlotItem):
    mouse_moved = QtCore.pyqtSignal(float, float)
    mouse_left_clicked = QtCore.pyqtSignal(float, float)
    range_changed = QtCore.pyqtSignal(list)

    def __init__(self, *args, **kwargs):
        super(ModifiedPlotItem, self).__init__(*args, **kwargs)

        self.modify_mouse_behavior()

    def modify_mouse_behavior(self):
        self.vb.mouseClickEvent = self.mouse_click_event
        self.vb.mouseDragEvent = self.mouse_drag_event
        self.vb.mouseDoubleClickEvent = self.mouse_double_click_event
        self.vb.wheelEvent = self.wheel_event
        self.range_changed_timer = QtCore.QTimer()
        self.range_changed_timer.timeout.connect(self.emit_sig_range_changed)
        self.range_changed_timer.setInterval(30)

        self.cur_mouse_position_x = 0
        self.cur_mouse_position_y = 0

        self.mouse_moved.connect(self.update_cur_mouse_position)
        self.last_view_range = np.array(self.vb.viewRange())

    def connect_mouse_move_event(self):
        self.scene().sigMouseMoved.connect(self.mouse_move_event)

    def mouse_move_event(self, pos):
        if self.sceneBoundingRect().contains(pos):
            pos = self.vb.mapSceneToView(pos)
            self.mouse_moved.emit(pos.x(), pos.y())

    def mouse_click_event(self, ev):
        if ev.button() == QtCore.Qt.RightButton or \
                (ev.button() == QtCore.Qt.LeftButton and
                         ev.modifiers() & QtCore.Qt.ControlModifier) and \
                self.vb.menuEnabled():
            ev.accept()
            self.vb.raiseContextMenu(ev)
        elif ev.button() == QtCore.Qt.LeftButton:
            if self.sceneBoundingRect().contains(ev.pos()):
                pos = self.vb.mapToView(ev.pos())
                x = pos.x()
                y = pos.y()
                self.mouse_left_clicked.emit(x, y)

    def update_cur_mouse_position(self, x, y):
        self.cur_mouse_position_x = x
        self.cur_mouse_position_y = y

    def get_mouse_position(self):
        return self.cur_mouse_position_x, self.cur_mouse_position_y

    def mouse_double_click_event(self, ev):
        if (ev.button() == QtCore.Qt.RightButton) or (ev.button() == QtCore.Qt.LeftButton and
                                                              ev.modifiers() & QtCore.Qt.ControlModifier):
            self.vb.autoRange()
            self.vb.enableAutoRange()
            self._auto_range = True
            self.vb.sigRangeChangedManually.emit(self.vb.state['mouseEnabled'])

    def mouse_drag_event(self, ev, axis=None):
        # most of this code is copied behavior mouse drag from the original code
        ev.accept()
        pos = ev.pos()
        last_pos = ev.lastPos()
        dif = pos - last_pos
        dif *= -1

        if ev.button() == QtCore.Qt.RightButton or \
                (ev.button() == QtCore.Qt.LeftButton and ev.modifiers() & QtCore.Qt.ControlModifier):
            # determine the amount of translation
            tr = dif
            tr = self.vb.mapToView(tr) - self.vb.mapToView(pg.Point(0, 0))
            x = tr.x()
            y = tr.y()
            self.vb.translateBy(x=x, y=y)
            if ev.start:
                self.range_changed_timer.start()
            if ev.isFinish():
                self.range_changed_timer.stop()
                self.emit_sig_range_changed()
        else:
            if ev.isFinish():  # This is the final move in the drag; change the view scale now
                self._auto_range = False
                self.vb.enableAutoRange(enable=False)
                self.vb.rbScaleBox.hide()
                ax = QtCore.QRectF(pg.Point(ev.buttonDownPos(ev.button())), pg.Point(pos))
                ax = self.vb.childGroup.mapRectFromParent(ax)
                self.vb.showAxRect(ax)
                self.vb.axHistoryPointer += 1
                self.vb.axHistory = self.vb.axHistory[:self.vb.axHistoryPointer] + [ax]
                self.vb.sigRangeChangedManually.emit(self.vb.state['mouseEnabled'])
            else:
                # update shape of scale box
                self.vb.updateScaleBox(ev.buttonDownPos(), ev.pos())

    def emit_sig_range_changed(self):
        new_view_range = np.array(self.vb.viewRange())
        if not np.array_equal(self.last_view_range, new_view_range):
            self.vb.sigRangeChangedManually.emit(self.vb.state['mouseEnabled'])
            self.last_view_range = new_view_range

    def wheel_event(self, ev, axis=None, *args):
        pg.ViewBox.wheelEvent(self.vb, ev, axis)
        self.vb.sigRangeChangedManually.emit(self.vb.state['mouseEnabled'])


class ModifiedLinearRegionItem(pg.LinearRegionItem):
    def __init__(self, *args, **kwargs):
        super(ModifiedLinearRegionItem, self).__init__()

    def mouseDragEvent(self, ev):
        return

    def hoverEvent(self, ev):
        return


class ModifiedScatterPlotItem(pg.ScatterPlotItem):
    def __init__(self, *args, **kwargs):
        super(ModifiedScatterPlotItem, self).__init__(*args, **kwargs)

    def mouseClickEvent(self, ev):
        return
