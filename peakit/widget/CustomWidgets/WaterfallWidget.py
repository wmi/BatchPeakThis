# -*- coding: utf8 -*-
from guidata.qt import QtGui, QtCore
import pyqtgraph as pg
import numpy as np
from scipy.interpolate import interp1d

pg.setConfigOption('useOpenGL', False)
pg.setConfigOption('leftButtonPan', False)
pg.setConfigOption('background', 'w')
pg.setConfigOption('foreground', 'k')
pg.setConfigOption('antialias', True)

plot_colors = {
    'data_pen': '#000',
    'data_brush': '#000',
    'roi_pen': '#f55',
    'roi_brush': '#fc0',
}


class WaterfallWidget(QtGui.QWidget):
    mouse_moved = QtCore.pyqtSignal(float, float)
    mouse_left_clicked = QtCore.pyqtSignal(float, float)
    range_changed = QtCore.pyqtSignal(list)

    def __init__(self, parent=None):
        super(WaterfallWidget, self).__init__(parent)

        self._layout = QtGui.QVBoxLayout()
        self._layout.setContentsMargins(0, 0, 0, 0)

        self._create_plots()
        self._create_mouse_position_widget()

        self.setLayout(self._layout)
        self.number_datasets = 0
        self.roi_width = 1
        self._create_plot_items()

#        self._waterfall_plot.connect_mouse_move_event()
#        self._waterfall_plot.mouse_moved.connect(self.update_mouse_position_widget)
#        self._waterfall_plot.mouse_left_clicked.connect(self.mouse_left_clicked.emit)
#        self._waterfall_plot.mouse_moved.connect(self.mouse_moved.emit)

    def _create_plots(self):
        self._pg_layout_widget = pg.GraphicsLayoutWidget()
        self._pg_layout = pg.GraphicsLayout()
        self._pg_layout.setContentsMargins(0, 0, 0, 0)
        self._pg_layout_widget.setContentsMargins(0, 0, 0, 0)

        self._waterfall_plot = pg.PlotItem()

        self._pg_layout.addItem(self._waterfall_plot, 0, 0)

        self._pg_layout.layout.setRowStretchFactor(0, 10)

        self._pg_layout_widget.addItem(self._pg_layout)
        self._layout.addWidget(self._pg_layout_widget)

    def _create_mouse_position_widget(self):
        self._pos_layout = QtGui.QHBoxLayout()
        self.x_lbl = QtGui.QLabel('x:')
        self.y_lbl = QtGui.QLabel('y:')

        self.x_lbl.setMinimumWidth(60)
        self.y_lbl.setMinimumWidth(60)

        self._pos_layout.addSpacerItem(QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.Expanding,
                                                         QtGui.QSizePolicy.Fixed))
        self._pos_layout.addWidget(self.x_lbl)
        self._pos_layout.addWidget(self.y_lbl)

        self._layout.addLayout(self._pos_layout)

    def data_plot_item_clicked(self):
        """
        this function is called when the data plot item is clicked and just emits the mouse_left_clicked signal
        x, y values to
        mouse_left_clicked. Otherwise the Data plot item blocks the signal...
        """
        x, y = self._spectrum_plot.get_mouse_position()
        self.mouse_left_clicked.emit(x, y)

    def update_mouse_position_widget(self, x, y):
        self.x_lbl.setText('x: {:02.4f}'.format(x))
        self.y_lbl.setText('y: {:02.4f}'.format(y))

    def _create_plot_items(self):
        self.current_selected_model = 0
        self._data_plot_item = pg.ScatterPlotItem(pen=pg.mkPen(plot_colors['data_pen'], width=1),
                                                 brush=pg.mkBrush(plot_colors['data_brush']),
                                                 clipToView=True)
        self.roi_item = pg.PolyLineROI([[0, 0], [0, 1]], closed=False)
        self.roi_item.setPen(pg.mkPen(color=plot_colors['roi_pen']))
#        self.roi_item.setBrush(pg.mkBrush(plot_colors['roi_brush']))
        self._waterfall_plot.addItem(self._data_plot_item)
        self._waterfall_plot.addItem(self.roi_item)

    def plot_lines(self, x, y):
        lines = MultiLine(x, y)
        self._waterfall_plot.addItem(lines)

    def plot_data_waterfall(self, measurement,
                            splitting=1, shift=1, normalize=True):
        """
        FIXME: add option to choose axis along which the cuts are performed
        """
        if np.shape(measurement.X)[0] == 0 or np.shape(measurement.X)[1] == 0:
            self._waterfall_plot.clear()
            return

        self.splitting = splitting
        self.normalize = normalize
        self.number_datasets = np.shape(measurement.Z)[0]

        x = measurement.X
        y = measurement.Y
        z = measurement.Z

        if self.normalize:
            z = (np.abs(z) - np.min(np.abs(z), axis=1)[:, np.newaxis])
            z /= np.max(np.abs(z), axis=1)[:, np.newaxis]

        offset = np.arange(0, self.number_datasets) * splitting
        z = z + offset[:, np.newaxis]

        y = y - shift*x
        self._waterfall_plot.disableAutoRange()
        self._waterfall_plot.clear()
        self.plot_lines(y, z)
        self._waterfall_plot.addItem(self.roi_item)
        self._waterfall_plot.autoRange()

        self.x = x
        self.y = y
        self.z = z

    def get_roi_centers(self):
        """
        Get center of the region of interest (ROI) for each data slice from the
        pyqtgraph ROI item.
        """
        
        handles = np.array(self.roi_item.saveState()['points'])
        roi_line = interp1d(handles[:, 1], handles[:, 0])
        y = np.arange(0, self.number_datasets) * self.splitting

        roi_line_e = extrap1d(roi_line)
        return roi_line_e(y)

    def get_rois(self, width):
        """
        Get upper and lower bound of the region of interest (ROI) in units of 
        the data.
        
        Params:
        =======
        
        width : numeric
            Width of ROI in units of the data.
        """
        roi_centers = self.get_roi_centers()
        rois = np.array([roi_centers - width/2.,
                         roi_centers + width/2.]).T
        return rois

    def get_rois_idx(self, width=1):
        """
        Return same length region of interest (ROI) as index tuple [min, max] 
        taking into account limits (length) of the data slices
        
        Params:
        =======
        
        width : int, optional
            Width (unitless, index) of the ROI.
        """
        # determine size of ROI in points (has to be equal for all slices)
        x = self.y[0, :]
        min_idx = np.argmin(np.abs(x - x[int(len(x)/2)] + width/2))
        max_idx = np.argmin(np.abs(x - x[int(len(x)/2)] - width/2))
        roi_width_idx = int(np.abs(max_idx-min_idx))

        roi_bounds = self.get_rois(width=width)
        rois = []
        for i, roi in enumerate(roi_bounds):
            # convert roi in units of y to index
            x = self.y[i, :]
            roi_idx = [np.argmin(np.abs(x - roi[0])),
                       np.argmin(np.abs(x - roi[1]))]
            roi_idx = np.sort(roi_idx)
            # ensure ROIs don't reach out of the y-range of the data
            rois.append(align_roi(roi_idx, roi_width_idx, len(x)))

        return rois

    def draw_rois(self, width=1):
        for i, roi in enumerate(self.get_rois(width=width)):
            pass


def extrap1d(interpolator):
    """
    Taken from http://stackoverflow.com/questions/2745329/how-to-make-scipy-
    interpolate-give-an-extrapolated-result-beyond-the-input-range
    """
    xs = interpolator.x
    ys = interpolator.y

    def pointwise(x):
        if x < xs[0]:
            return ys[0]+(x-xs[0])*(ys[1]-ys[0])/(xs[1]-xs[0])
        elif x > xs[-1]:
            return ys[-1]+(x-xs[-1])*(ys[-1]-ys[-2])/(xs[-1]-xs[-2])
        else:
            return interpolator(x)

    def ufunclike(xs):
        return np.array(list(map(pointwise, np.array(xs))))

    return ufunclike


class MultiLine(pg.QtGui.QGraphicsPathItem):
    """ 
    For more performance than ScatterPlotItems according to
    http://stackoverflow.com/questions/17103698/plotting-large-arrays-in-pyqtgraph
    """
    def __init__(self, x, y):
        """x and y are 2D arrays of shape (Nplots, Nsamples)"""
        connect = np.ones(x.shape, dtype=bool)
        connect[:, -1] = 0  # don't draw the segment between each trace
        self.path = pg.arrayToQPath(x.flatten(), y.flatten(), connect.flatten())
        pg.QtGui.QGraphicsPathItem.__init__(self, self.path)
        self.setPen(pg.mkPen(color=plot_colors['data_pen'], width=1))

    def shape(self):
        # override shape because QGraphicsPathItem.shape is too expensive.
        return pg.QtGui.QGraphicsItem.shape(self)

    def boundingRect(self):
        return self.path.boundingRect()

def align_roi(roi, roi_width, len_x):
    """
    Align region of interest (ROI) with limits of data so that the width of 
    the ROI always stays the same. This function acts on index-ROIs
    
    Params:
    =======
    
    roi : [int, int]
        ROI bounds to align (unitless index) [min, max]
    roi_width :
        Width of the ROI (unitless index)
    len_x :
        Length (indices) of the data
        
    """
    roi_aligned = [0, roi_width]
    if roi[0] <= 0:
        roi_aligned[0] = 0
        roi_aligned[1] = 0 + roi_width
    elif roi[1] == len_x-1:
        roi_aligned[0] = roi[1] - roi_width
        roi_aligned[1] = roi[1]
    else:
        roi_aligned[0] = roi[0]
        roi_aligned[1] = roi[0] + roi_width

    return roi_aligned
