# -*- coding: utf-8 -*-
import sys
import os
from guidata.qt import QtCore, QtGui

import sympy as s
from peakit.controller.SingleController import SingleController
from peakit.widget.ControlWidgets import (BatchFitWidget, DataNavWidget,
                                          AutomationWidget, VisualizeFitWidget,
                                          SessionWidget)
from peakit.widget.CustomWidgets import ExpandableBox
from bbfmr.measurement import Measurement

class MeasurementWidget(QtGui.QMainWindow):
    measurement_changed = QtCore.pyqtSignal(Measurement)

    def __init__(self, single_widget, version=None, parent=None):
        super(MeasurementWidget, self).__init__(parent)

        self.main_splitter = QtGui.QSplitter()

        self.single_widget = single_widget
        self.control_widget = ControlWidget(self)
        self.control_widget.setMinimumWidth(250)

        self.main_splitter.addWidget(self.single_widget)
        self.main_splitter.addWidget(self.control_widget)

        self.load_stylesheet()

        self.setCentralWidget(self.main_splitter)
        self.main_splitter.setStretchFactor(0, 100)
        self.main_splitter.setStretchFactor(1, 1)
        self.main_splitter.setStretchFactor(2, 1)
        self.main_splitter.setCollapsible(0, False)
        self.main_splitter.setCollapsible(1, False)

        self.setWindowTitle("PeakThis v{}".format(version))
        self.set_shortcuts()

    def set_shortcuts(self):
        self.fit_btn = self.control_widget.fit_widget.fit_btn

    def load_stylesheet(self):
        stylesheet_file = open(os.path.join(module_path(), "DioptasStyle.qss"),
                               'r')
        stylesheet_str = stylesheet_file.read()
        self.setStyleSheet(stylesheet_str)

    def show(self):
        QtGui.QWidget.show(self)
        self.setWindowState((self.windowState() & ~
                            QtCore.Qt.WindowMinimized |
                            QtCore.Qt.WindowActive))
        self.activateWindow()
        self.raise_()


class ControlWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(ControlWidget, self).__init__(parent)
        self.main_vertical_layout = QtGui.QVBoxLayout()
        self.main_vertical_layout.setContentsMargins(0, 5, 5, 5)
        self.main_vertical_layout.setSpacing(5)

        self.nav_widget = DataNavWidget(self)
        self.session_widget = SessionWidget(self)
        self.visualize_widget = VisualizeFitWidget(self)
        self.automation_widget = AutomationWidget(self)
        self.fit_widget = BatchFitWidget(self)

        self.main_vertical_layout.addWidget(ExpandableBox(self.session_widget,
                                                          "Session management"))
        self.main_vertical_layout.addWidget(ExpandableBox(self.nav_widget,
                                                          "Dataset navigation"))
        self.main_vertical_layout.addWidget(ExpandableBox(self.visualize_widget,
                                                          "Visualize fit params"))
        self.main_vertical_layout.addWidget(ExpandableBox(self.automation_widget,
                                                          "Automation"))
        self.main_vertical_layout.addWidget(ExpandableBox(self.fit_widget,
                                                          "Batch fit"))

        self.main_vertical_layout.addSpacerItem(QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.Fixed,
                                                                  QtGui.QSizePolicy.Expanding))

        self.setLayout(self.main_vertical_layout)

    def disable(self, except_widgets=None):
        for control_box in self.children():
            try:
                control_box.enable_widgets(False)
            except AttributeError:
                pass

        for widget in except_widgets:
            widget.setEnabled(True)

    def enable(self):
        for control_box in self.children():
            try:
                control_box.enable_widgets(True)
            except AttributeError:
                pass




def we_are_frozen():
    # All of the modules are built-in to the interpreter, e.g., by py2exe
    return hasattr(sys, "frozen")


def module_path():
    if we_are_frozen():
        return os.path.dirname(sys.executable)
    return os.path.dirname(__file__)

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    main_view = MeasurementWidget(SingleController().main_widget)
    main_view.show()
    app.exec_()