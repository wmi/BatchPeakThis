# -*- coding: utf8 -*-
import sys
import os
from guidata.qt import QtCore, QtGui

from peakit.widget.CustomWidgets import WaterfallWidget
from peakit.widget.ControlWidgets.WaterfallControlWidget import WaterfallControlWidget
from peakit.widget.ControlWidgets.RoiWidget import RoiWidget
from peakit.widget.CustomWidgets import ExpandableBox, FlatButton
import numpy as np
from bbfmr.measurement import Measurement
from bbfmr.processing import limit_rois


class WaterfallDialog(QtGui.QDialog):
    def __init__(self, parent=None, data=None):
        super(WaterfallDialog, self).__init__(parent)
        self.setWindowFlags(self.windowFlags() |
                            QtCore.Qt.WindowSystemMenuHint |
                            QtCore.Qt.WindowMinMaxButtonsHint)
        self.main_layout = QtGui.QVBoxLayout()
        self.main_splitter = QtGui.QSplitter()

        self.waterfall_widget = WaterfallWidget(self)
        self.control_widget = ControlWidget(self)
        self.control_widget.setMinimumWidth(250)

        self.main_splitter.addWidget(self.waterfall_widget)
        self.main_splitter.addWidget(self.control_widget)

        self.load_stylesheet()

        self.main_splitter.setStretchFactor(0, 100)
        self.main_splitter.setStretchFactor(1, 0)
        self.main_splitter.setCollapsible(0, False)
        self.main_splitter.setCollapsible(1, False)

        self.setWindowTitle("PeakThis - Select fitting range")

        self.main_layout.addWidget(self.main_splitter)
        self.setLayout(self.main_layout)

        self.set_shortcuts()
        self.create_subscriptions()
        if data:
            self.set_data(data)

    def set_data(self, data):
        if issubclass(type(data), Measurement):
            self.data = data
        elif type(data) is np.ndarray:
            self.data = Measurement(data)
        else:
            return ValueError("Input data not understood. Supply Measurement\
            object or x,y,z data as array")

    def set_shortcuts(self):
        self.splitting_slider = self.control_widget.waterfall_control_widget.splitting_slider
        self.shift_edit = self.control_widget.waterfall_control_widget.shift_edit
        self.roi_width = self.control_widget.roi_widget.roi_width
        self.re = self.control_widget.roi_widget.reset_roi_btn

    def create_subscriptions(self):
        self.splitting_slider.valueChanged.connect(self.plot_waterfall)
        self.shift_edit.textChanged.connect(self.plot_waterfall)
        self.control_widget.roi_widget.reset_roi_btn.clicked.connect(self.clear_limits)
        self.control_widget.roi_widget.apply_roi_btn.clicked.connect(self.limit_rois)
        self.control_widget.roi_widget.reset_last_roi_btn.clicked.connect(self.clear_last_limit)

    def load_stylesheet(self):
        stylesheet_file = open(os.path.join(module_path(),
                                            "DioptasStyle.qss"), 'r')
        stylesheet_str = stylesheet_file.read()
        self.setStyleSheet(stylesheet_str)

    def show(self):
        QtGui.QWidget.show(self)
        self.setWindowState(self.windowState() & ~
                            QtCore.Qt.WindowMinimized |
                            QtCore.Qt.WindowActive)
        self.activateWindow()
        self.raise_()

    def get_rois_idx(self):
        """
        Get region of interest for each slice of the data as tuple of
        [min_idx, max_idx].
        """
        return self.waterfall_widget.get_rois_idx(
                    width=float(self.roi_width.text())
                    )

    def plot_waterfall(self):
        splt = (self.splitting_slider.value() /
                self.control_widget.waterfall_control_widget.slider_steps)
        shft = float(self.shift_edit.text())
        self.waterfall_widget.plot_data_waterfall(self.data,
                                                  splitting=splt,
                                                  shift=shft,
                                                  normalize=True)

    def limit_rois(self):
        self.data.add_operation(limit_rois, rois=list(self.get_rois_idx()))
        self.plot_waterfall()

    def clear_last_limit(self):
        idx = self.data.find_operation(limit_rois)
        if idx:
            self.data.operations.pop(idx[-1])
        self.data.process()
        self.plot_waterfall()

    def clear_limits(self):
        for idx in np.sort(self.data.find_operation(limit_rois))[::-1]:
            self.data.operations.pop(idx)
        self.data.process()
        self.plot_waterfall()


class ControlWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(ControlWidget, self).__init__(parent)
        self.main_vertical_layout = QtGui.QVBoxLayout()
        self.main_vertical_layout.setContentsMargins(0, 5, 5, 5)
        self.main_vertical_layout.setSpacing(5)

        self.waterfall_control_widget = WaterfallControlWidget(self)
        self.roi_widget = RoiWidget(self)

        box_waterfall_control = ExpandableBox(self.waterfall_control_widget,
                                              "Waterfall settings")
        box_roi_widget = ExpandableBox(self.roi_widget,
                                       "Region of interest")

        self.main_vertical_layout.addWidget(box_waterfall_control)
        self.main_vertical_layout.addWidget(box_roi_widget)
        self.main_vertical_layout.addSpacerItem(QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.Fixed,
                                                                  QtGui.QSizePolicy.Expanding))

        self.setLayout(self.main_vertical_layout)

    def disable(self, except_widgets=None):
        for control_box in self.children():
            try:
                control_box.enable_widgets(False)
            except AttributeError:
                pass

        for widget in except_widgets:
            widget.setEnabled(True)

    def enable(self):
        for control_box in self.children():
            try:
                control_box.enable_widgets(True)
            except AttributeError:
                pass


def we_are_frozen():
    # All of the modules are built-in to the interpreter, e.g., by py2exe
    return hasattr(sys, "frozen")


def module_path():
    if we_are_frozen():
        return os.path.dirname(sys.executable)
    return os.path.dirname(__file__)

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    main_view = WaterfallWidget()
    main_view.show()
    app.exec_()