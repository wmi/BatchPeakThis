# -*- coding: utf8 -*-
import copy
import os
import numpy as np
from guidata.qt import QtCore, QtGui
import jsonpickle
from collections import OrderedDict
from bbfmr.complex_model import ComplexModel

from peakit.widget.SingleWidget import SingleWidget
from peakit.model.DataModel import DataModel
from peakit.model.PickModels import models_dict
from peakit.helper.peakdetect import peakdetect

class NumpyFloatHandler(jsonpickle.handlers.BaseHandler):
    """
    Automatic conversion of numpy float  to python floats
    Required for jsonpickle to work correctly
    """
    def flatten(self, obj, data):
        """
        Converts and rounds a Numpy.float* to Python float
        """
        return round(obj,6)

jsonpickle.handlers.registry.register(np.float, NumpyFloatHandler)
jsonpickle.handlers.registry.register(np.float32, NumpyFloatHandler)
jsonpickle.handlers.registry.register(np.float64, NumpyFloatHandler)

class SingleController(object):
    def __init__(self, data=None, labels=None, standalone=True):
        self.standalone = standalone
        self.main_widget = SingleWidget()
        self.data = DataModel()
        if data is None:
            x = np.linspace(0, 30, 1000)
            y = np.sin(x) + 1 + 1j*np.cos(x)
            self.set_data(x, y)
        else:
            self.set_data(*data)
        if labels:
            self.update_labels(*labels)

        self.create_subscriptions()

        self.save_data_path = ''

    def show_view(self):
        self.main_widget.show()

    def create_subscriptions(self):
        ######################################
        # %% File Signals
        self.main_widget.load_file_btn.clicked.connect(self.load_data)
        self.main_widget.save_data_btn.clicked.connect(self.save_data)
        self.main_widget.save_models_btn.clicked.connect(self.save_fit)
        self.main_widget.load_models_btn.clicked.connect(self.load_fit)

        ######################################
        # %% Data signals
        self.data.spectrum_changed.connect(self.plot_data_spectrum)
        self.data.background_changed.connect(self.main_widget.spectrum_widget.plot_background_spectrum)
        self.data.background_points_changed.connect(self.main_widget.spectrum_widget.plot_background_points_spectrum)

        self.data.models_changed.connect(self.update_displayed_models)

        #####################################
        # %% Gui Model signals
        self.main_widget.model_add_btn.clicked.connect(self.add_model_btn_clicked)
        self.main_widget.control_widget.model_widget.model_selector_dialog.accepted.connect(
            self.add_model_dialog_accepted
        )
        self.main_widget.model_copy_btn.clicked.connect(
            self.copy_model_btn_clicked
        )
        self.main_widget.control_widget.model_widget.model_selected_changed.connect(
            self.update_displayed_model_parameters
        )
        self.main_widget.control_widget.model_widget.model_selected_changed.connect(
            self.main_widget.spectrum_widget.activate_model_spectrum
        )
        self.main_widget.control_widget.model_widget.model_parameters_changed.connect(
            self.data.update_model
        )
        self.data.model_parameters_changed.connect(
            self.main_widget.spectrum_widget.update_model_spectrum
        )
        self.data.model_parameters_changed.connect(
            self.update_displayed_model_parameters
        )
        self.data.model_sum_changed.connect(
            self.main_widget.spectrum_widget.plot_model_sum_spectrum
        )
        self.main_widget.model_define_btn.clicked.connect(
            self.start_model_picking
        )
        self.main_widget.model_delete_btn.clicked.connect(
            self.del_model_btn_clicked
        )
        self.main_widget.control_widget.model_widget.model_list.doubleClicked.connect(
            self.model_settings
        )
        self.main_widget.control_widget.model_widget.model_settings_dialog.accepted.connect(
            self.model_settings_dialog_accepted
        )

        ##############################################
        # %% Fitting signals
        self.main_widget.peakdetect_btn.clicked.connect(self.peakdetect)
        self.main_widget.initialize_btn.clicked.connect(self.data.initialize_fit)
        self.main_widget.fit_btn.clicked.connect(self.data.fit_data)
        self.data.residual_changed.connect(self.main_widget.spectrum_widget.plot_residual_spectrum)

        ##############################################
        # %% ROI Signals
        self.main_widget.spectrum_widget.linear_region_item.sigRegionChangeFinished.connect(self.roi_item_changed)
        self.data.roi_value_changed.connect(self.roi_value_changed)

        ##############################################
        # Background widget controls
        self.main_widget.background_define_btn.clicked.connect(self.start_background_picking)
        self.main_widget.background_method_cb.currentIndexChanged.connect(self.background_model_changed)
        self.main_widget.background_subtract_btn.toggled.connect(self.data.set_background_subtracted)

        self.main_widget.closeEvent = self.close_event
        
    def set_data(self, x, y):
        self.data.set_spectrum_data(x, y)
        self.plot_data_spectrum()

    def plot_data_spectrum(self):
        self.main_widget.spectrum_widget.plot_data(*self.data.get_whole_spectrum().data)
        self.main_widget.spectrum_widget.plot_roi_data(*self.data.get_spectrum().data)
        self.update_labels(self.data.spectrum.xlabel, self.data.spectrum.ylabel)

    def start_background_picking(self):
        self.main_widget.background_define_btn.clicked(self.end_background_picking)

        self.main_widget.background_define_btn.setText('Finish')
        self.main_widget.control_widget.disable(except_widgets=[self.main_widget.background_define_btn,
                                                                self.main_widget.background_subtract_btn,
                                                                self.main_widget.background_method_cb])

        self.main_widget.spectrum_widget.set_spectrum_plot_keypress_callback(
            self.spectrum_key_press_event_background_picking)
        self.main_widget.spectrum_widget.set_spectrum_plot_focus()

        self.main_widget.spectrum_widget.mouse_left_clicked.connect(self.data.add_background_model_point)

    def end_background_picking(self):
        self.main_widget.background_define_btn.clicked(self.start_background_picking)

        self.main_widget.background_define_btn.setText('Define')
        self.main_widget.control_widget.enable()

        self.main_widget.spectrum_widget.set_spectrum_plot_keypress_callback(
            self.spectrum_key_press_event_empty)

        self.main_widget.spectrum_widget.mouse_left_clicked.disconnect(self.data.add_background_model_point)

    def background_model_changed(self):
        self.data.background_model.set_method(str(self.main_widget.background_method_cb.currentText()))


    def spectrum_key_press_event_background_picking(self, QKeyEvent):
        if QKeyEvent.text() == 'x':
            mouse_x, mouse_y = self.main_widget.spectrum_widget.get_mouse_position()
            self.data.remove_background_model_point_close_to(mouse_x, mouse_y)

    def spectrum_key_press_event_empty(self, QKeyEvent):
        pass

    def add_model_btn_clicked(self):
        suitable_models_dict = OrderedDict()
        for k, m in models_dict.items():
            if self.data.get_spectrum().iscomplex():
                if issubclass(m, ComplexModel):
                    suitable_models_dict[k] = m
            else:
                if not issubclass(m, ComplexModel):
                    suitable_models_dict[k] = m

        self.main_widget.model_selector_dialog.populate_models(suitable_models_dict)
        self.main_widget.control_widget.model_widget.show_model_selector_dialog()

    def del_model_btn_clicked(self):
        cur_ind = self.main_widget.model_list.currentRow()
        if cur_ind != -1:
            self.main_widget.model_list.takeItem(cur_ind)
            self.main_widget.spectrum_widget.del_model(cur_ind)
            self.data.del_model(cur_ind)

    def add_model_dialog_accepted(self):
        selected_name = self.main_widget.model_selector_dialog.get_selected_item_string()
        self.data.add_model(models_dict[selected_name]())
        self.main_widget.spectrum_widget.add_model(self.data.get_model_spectrum(-1))
        self.main_widget.control_widget.model_widget.model_list.setCurrentRow(len(self.data.models) - 1)

    def copy_model_btn_clicked(self):
        cur_ind = self.main_widget.model_list.currentRow()
        if cur_ind != -1:
            self.data.add_model(copy.deepcopy(self.data.models[cur_ind]))
            self.main_widget.spectrum_widget.add_model(self.data.get_model_spectrum(-1))
            self.main_widget.control_widget.model_widget.model_list.setCurrentRow(len(self.data.models) - 1)

    def update_displayed_models(self):
        self.main_widget.model_list.blockSignals(True)
        self.main_widget.control_widget.model_widget.model_list.clear()
        self.main_widget.spectrum_widget.current_selected_model = 0
        for model in self.data.models:
            self.main_widget.control_widget.model_widget.model_list.addItem(model.name)
        self.main_widget.model_list.blockSignals(False)
        self.update_plotted_models()

    def update_displayed_model_parameters(self, index):
        self.main_widget.control_widget.model_widget.update_parameters(self.data.models[index].parameters)

    def model_settings(self):
        # FIXME: set spin box to id of currently selected model
        self.main_widget.control_widget.model_widget.show_model_settings_dialog()

    def model_settings_dialog_accepted(self):
        sel_id = int(self.main_widget.control_widget.model_widget.model_settings_dialog.get_selected_id())
        current_ind = self.main_widget.model_list.currentRow()
        if current_ind != -1:
            cur_id = int(self.data.models[current_ind].prefix.replace("id", "").replace("_",""))
            for i, model in enumerate(self.data.models):
                if model.prefix == "id%d_" % sel_id:
                    model.set_prefix("id%d_" % cur_id)
                    break
            self.data.models[current_ind].set_prefix("id%d_" % sel_id)

        self.update_displayed_models()
        self.update_displayed_model_parameters(current_ind)


    def start_model_picking(self):
        if self.main_widget.model_list.currentRow()==-1:
            return
        self.main_widget.model_define_btn.clicked(self.end_model_picking)
        self.main_widget.model_define_btn.setText("Finish")
        self.main_widget.control_widget.disable(except_widgets=[self.main_widget.model_define_btn])

        self.main_widget.spectrum_widget.mouse_moved.connect(self.update_model_parameters)
        self.main_widget.spectrum_widget.mouse_left_clicked.connect(self.pick_model_parameter)

    def end_model_picking(self):
        self.main_widget.model_define_btn.clicked(self.start_model_picking)
        self.main_widget.model_define_btn.setText("Define")
        self.main_widget.model_define_btn.setChecked(False)
        self.main_widget.control_widget.enable()
        self.main_widget.spectrum_widget.mouse_moved.disconnect(self.update_model_parameters)
        self.main_widget.spectrum_widget.mouse_left_clicked.disconnect(self.pick_model_parameter)

    def update_model_parameters(self, x, y):
        cur_model_ind = int(self.main_widget.model_list.currentRow())
        self.data.update_current_model_parameter(cur_model_ind, x, y)

    def pick_model_parameter(self, x, y):
        cur_model_ind = int(self.main_widget.model_list.currentRow())
        more_parameters_available = self.data.pick_current_model_parameters(cur_model_ind, x, y)
        if not more_parameters_available:
            self.end_model_picking()

    def update_plotted_models(self):
        """ (Re-)Plot all currently added models """
        self.main_widget.spectrum_widget.clear_models()
        for i, _ in enumerate(self.data.models):
            self.main_widget.spectrum_widget.add_model(self.data.get_model_spectrum(i))
        self.main_widget.spectrum_widget.plot_model_sum_spectrum(self.data.get_model_sum_spectrum())

    def update_labels(self, xlabel, ylabel):
        self.main_widget.spectrum_widget.set_labels(xlabel, ylabel)

    ##############################################
    # Save/recall state
    def save_fit(self, filename=None, overwrite=False):
        if self.data.fit is None:
            raise Exception("No fit has succeeded yet.")

        if filename is None:
            filename = str(QtGui.QFileDialog.getSaveFileName(
                self.main_widget,
                "Save Models",
                self.data.spectrum.title + '.models.json'))
        elif os.path.isfile(filename) and not overwrite:
            raise(FileExistsError)

        if filename is not '':
            data = {"params": self.data.fit.params,
                    "models": self.data.get_fit_model_classes(),
                    "prefixes": [m.prefix for m in self.data.models]
                    }

            with open(filename, "w") as f:
                f.write(jsonpickle.dumps(data, f))

    def load_fit(self, filename=None, emit=True):
        if filename is None:
            filename = str(QtGui.QFileDialog.getOpenFileName(
                self.main_widget,
                "Load Fit",
                "",
                filter="*.models.json"))
        if filename is not '':
            fit = self.data.load_fit(filename)
            self.update_displayed_models()
            self.data.models_changed.emit()
            self.data.model_sum_changed.emit(self.data.get_model_sum_spectrum())
            return fit
        else:
            return None

    def load_data(self, filename=None):
        if filename is None:
            filename = str(QtGui.QFileDialog.getOpenFileName(
                self.main_widget, "Load Data File", ''))
        if filename is not '':
            self.data.load_data(filename)

    def save_data(self, filename=None, overwrite=False):
        if filename is None:
            filename = str(QtGui.QFileDialog.getSaveFileName(
                self.main_widget,
                "Save Data",
                self.data.spectrum.title + '.dat'))
        elif os.path.isfile(filename) and not overwrite:
            raise(FileExistsError)

        if filename is not '':
            self.data.save_data(filename)
            self.save_data_path = os.path.dirname(filename)


    ##############################################
    # Peakdetect
    def peakdetect(self):
        x, y = self.data.get_spectrum_data()
        if y.dtype == complex:
            y_det = np.abs(y)
        else:
            y_det = y

        lookahead = int(self.main_widget.control_widget.peak_widget.lookahead_edit.text())
        delta = float(self.main_widget.control_widget.peak_widget.delta_edit.text())
        max_peaks, min_peaks = peakdetect(y_det, x, lookahead=lookahead, delta=delta)
        all_peaks=[max_peaks, min_peaks]
        self.peaks = all_peaks[self.main_widget.control_widget.peak_widget.minmax_combo.currentIndex()]

        if y.dtype == complex:
            # recover signal values at peak position
            for i, peak in enumerate(self.peaks):
                x_idx = np.argmin(np.abs(x-peak[0]))
                self.peaks[i][1] = y[x_idx]

        self.main_widget.spectrum_widget.plot_peak_points(*zip(*self.peaks))

        i = 0
        for model in self.data.models:
            try:
                print(model.parameters)
                model.parameters[model.prefix + "f_r"].value = self.peaks[i][0]
                i += 1
            except KeyError:
                pass

        self.update_displayed_model_parameters(int(self.main_widget.model_list.currentRow()))
        self.update_displayed_models()

    def clear_peakdetect(self):
        self.peaks = []
        self.main_widget.spectrum_widget.clear_peaks()

    ##############################################
    # Region of interest (ROI)
    def roi_item_changed(self):
        x_min, x_max = self.main_widget.spectrum_widget.linear_region_item.getRegion()
        self.data.set_roi(x_min, x_max, emit=False)

    def roi_value_changed(self, x_min, x_max):
        self.main_widget.spectrum_widget.linear_region_item.blockSignals(True)
        self.main_widget.spectrum_widget.linear_region_item.setRegion([x_min, x_max])
        self.main_widget.spectrum_widget.linear_region_item.blockSignals(False)

    def close_event(self, _):
        if self.standalone is True:
            QtGui.QApplication.closeAllWindows()
            QtGui.QApplication.quit()