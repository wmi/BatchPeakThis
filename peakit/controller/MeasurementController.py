# -*- coding: utf-8 -*-
import copy
import os
import numpy as np
from guidata.qt import QtCore, QtGui
from guidata.qt.QtGui import QFileDialog
from glob import glob
import jsonpickle

from peakit.controller.SingleController import SingleController
from peakit.widget.MeasurementWidget import MeasurementWidget
from bbfmr.measurement import Measurement
from bbfmr.processing import cut
from bbfmr.gui.interactive_plotting import QFileDialog_compatibility

def demo_measurement():
    x = np.linspace(-3, 3, num=50)
    y = np.linspace(-2, 2, num=80)
    y, x = np.meshgrid(y, x)
    z = ((1 - x / 2. + x ** 5 + y ** 3) * np.exp(-x ** 2 - y ** 2)*1 +
         (1 - x / 5. + x ** 3 + y ** 6) * np.exp(-x ** 2 - y ** 2)*1j)
    m = Measurement(X=x, Y=y, Z=z)
    m.metadata["title"] = "test-measurement"
    return m

class MeasurementController(object):
    def __init__(self, measurement=None):
        self.single_controller = SingleController(standalone=False)
        self.main_widget = MeasurementWidget(self.single_controller.main_widget,
                                             version=0.1)
        self.set_shortcuts()
        self.create_subscriptions()

        self.cur_dataset_param = 1.
        if measurement is None:
            self.measurement = demo_measurement()
        else:
            self.measurement = measurement
        self.cur_dataset_idx = 0
        self.fit_queue = []

    def set_shortcuts(self):
        self.sw = self.single_controller.main_widget
        self.ses_w = self.main_widget.control_widget.session_widget
        self.nav_w = self.main_widget.control_widget.nav_widget
        self.vis_w = self.main_widget.control_widget.visualize_widget
        self.fit_w = self.main_widget.control_widget.fit_widget
        self.aut_w = self.main_widget.control_widget.automation_widget

    def create_subscriptions(self):
        self.main_widget.measurement_changed.connect(
            self.update_datasets_metadata)
        self.single_controller.data.models_changed.connect(
            self.update_parameter_names)
        self.single_controller.data.fit_succeeded.connect(
            self.fit_succeeded)
        self.vis_w.visualize_btn.clicked.connect(self.visualize_fit)
        self.vis_w.visualize_x.currentIndexChanged.connect(self.visualize_fit_inline)
        self.vis_w.visualize_y.currentIndexChanged.connect(self.visualize_fit_inline)
        self.vis_w._data_item.sigClicked.connect(self.select_dataset_fit_param)
        self.vis_w.delete_fits_btn.clicked.connect(self.delete_visible_fits)
        self.nav_w.next_btn.clicked.connect(self.next_dataset)
        self.nav_w.prev_btn.clicked.connect(self.prev_dataset)
        self.nav_w.reset_btn.clicked.connect(self.reset_fit)
        self.nav_w.datasets_slider.valueChanged.connect(self.select_dataset)
        self.ses_w.save_session_btn.clicked.connect(self.save_session)
        self.ses_w.load_session_btn.clicked.connect(self.load_session)
        self.ses_w.save_measurement_btn.clicked.connect(self.save_measurement)
        self.ses_w.load_measurement_btn.clicked.connect(self.load_measurement)
        self.nav_w.waterfall_btn.clicked.connect(self.waterfall_dialog)
        self.nav_w.waterfall_dialog.accepted.connect(
            self.waterfall_dialog_accepted
        )
        self.nav_w.waterfall_dialog.rejected.connect(
            self.waterfall_dialog_accepted
        )
        self.fit_w.fit_btn.clicked.connect(self.fit_all)

    def show_view(self):
        self.main_widget.show()

    def update_parameter_names(self):
        _, params = self.single_controller.data.combine_models()
        param_names = list(params.keys())
        param_names = ["index", self.measurement.metadata["xlabel"]] + \
                      sorted(param_names)

        last_x_idx = self.vis_w.visualize_x.currentIndex()
        last_y_idx = self.vis_w.visualize_y.currentIndex()
        self.vis_w.visualize_x.clear()
        self.vis_w.visualize_y.clear()
        self.vis_w.visualize_x.addItems(param_names)
        self.vis_w.visualize_y.addItems(param_names)
        self.vis_w.visualize_x.setCurrentIndex(last_x_idx)
        self.vis_w.visualize_y.setCurrentIndex(last_y_idx)

    ######################################
    # %% Measurement management
    @property
    def measurement(self):
        return self._measurement

    @measurement.setter
    def measurement(self, m):
        if m is None:
            self._measurement = None
            return
        self._measurement = m
        self._measurement.process()
        self.main_widget.measurement_changed.emit(self.measurement)
        
        self.fits = [None]*self.count_datasets()
        self.rois = [None]*self.count_datasets()

    def update_datasets_metadata(self, m):
        """
        Update metadata about measurement datasets in frontend (limits for
        slider, spinbox, ... labels, name in SingleWidget)
        """
        n_datasets = self.count_datasets()
        self.nav_w.current_label.setText("(%s = %.2e)" %
                                         (self.measurement.metadata["xlabel"],
                                          self.cur_dataset_param))
        self.nav_w.current_spin.setRange(0, n_datasets-1)
        self.nav_w.datasets_slider.setRange(0, n_datasets-1)

        spectrum =  self.single_controller.data.spectrum
        spectrum.title = self.measurement.metadata["title"]
        spectrum.xlabel = self.measurement.metadata["ylabel"]
        spectrum.ylabel  = self.measurement.metadata["zlabel"]

    ######################################
    # %% Dataset navigation
    @property
    def cur_dataset_idx(self):
        return self._cur_dataset_idx

    @cur_dataset_idx.setter
    def cur_dataset_idx(self, idx):
        self._cur_dataset_idx = idx
        cut_idx = self.measurement.find_operation(cut)
        if not len(cut_idx) or cut_idx[0] == len(self.measurement.operations):
            x, y, z = self.measurement.run_operation(cut, x_idx=idx)
        else:
            self.measurement.replace_operation(cut, cut, x_idx=idx)
            self.measurement.process()
            x = self.measurement.X
            y = self.measurement.Y
            z = self.measurement.Z
        x, y, param = self.squeeze_measurement_data(x, y, z)
        self.cur_dataset_param = float(param)

        # Recall fits and models
        if self.fits[idx] is not None:
            self.single_controller.data.fit = self.fits[idx]
            self.single_controller.data.recall_models(self.fits[idx])

        # Populate data for slice with current index
        self.single_controller.data.set_spectrum_data(x, y, emit=False)
        # Recall roi
        if self.rois[idx] is not None:
            self.single_controller.data.roi = self.rois[idx]
        else:
            self.single_controller.data.roi = np.arange(0, len(x))

        # Trigger plotting the data
        self.single_controller.data.spectrum_changed.emit(
            self.single_controller.data.get_spectrum())

        # Update dataset information
        self.nav_w.current_label.setText("(%s = %.2e)" %
                                         (self.measurement.metadata["xlabel"],
                                          self.cur_dataset_param))

        # Update inline fit visualization (FIXME: this is overkill, only the
        # current marker needs to be refreshed)
        self.visualize_fit_inline()

    def reset_fit(self):
        self.fits[self.cur_dataset_idx] = None

    def next_dataset(self):
        cur = self.nav_w.datasets_slider.value()
        self.nav_w.datasets_slider.setValue(cur + 1)

    def prev_dataset(self):
        cur = self.nav_w.datasets_slider.value()
        self.nav_w.datasets_slider.setValue(cur-1)

    def select_dataset(self, idx):
        self.cur_dataset_idx = idx

    def count_datasets(self):
        """
        how many slices (datasets) there are in the measurement
        """
        idx_cut = self.measurement.find_operation(cut)
        if idx_cut:
            c = self.measurement.operations.pop(idx_cut[0])
            self.measurement.process()
            n = np.shape(self.measurement.X.squeeze())[0] # x-len
            self.measurement.operations.insert(idx_cut[0], c)
            self.measurement.process()
        else:
            n = np.shape(self.measurement.X.squeeze())[0] # x-len

        return n

    def waterfall_dialog(self):
        self.nav_w.waterfall_dialog.set_data(self.measurement)
        self.nav_w.waterfall_dialog.plot_waterfall()
        self.nav_w.waterfall_dialog.show()

    def waterfall_dialog_accepted(self):
        self.measurement = self.measurement
        self.single_controller.data.roi = None
        self.cur_dataset_idx = self.cur_dataset_idx

    def squeeze_measurement_data(self, X, Y, Z):
        if np.shape(X)[0] > 1 and np.shape(X)[1] > 1:
            raise ValueError("Must present 1D data to program")
        elif np.shape(X)[0] == 1:
            x = Y.squeeze()
            y = Z.squeeze()
            param = X.squeeze()[0]
        elif np.shape(X)[1] == 1:
            x = X.squeeze()
            y = Z.squeeze()
            param = Y.squeeze()[0]
        return x, y, param

    def get_index_value(self):
        """ Get the current value where the cut of the Measurement is performed
        """
        return self.measurement.X[:, 0]

    ######################################
    # %% Fitting
    def fit_all(self):
        if len(self.fit_queue) != 0:
            # Abort batch fit progress
            self.fit_queue = []
            self.fit_w.fit_btn.setText("Fit all")
            return
        self.fit_queue = list(np.arange(self.cur_dataset_idx,
                                        self.count_datasets()))
        self.fit_w.fit_btn.setText("Abort batch fit")
        self.fit()

    def fit(self, wait=50):
        """
        Trigger fitting of one (or sequentially multiple) dataset(s) by index
        """
        if len(self.fit_queue) == 0:
            self.fit_w.fit_btn.setText("Fit all")
            return True

        idx = self.fit_queue.pop(0)
        self.nav_w.datasets_slider.setValue(idx)
        self.prepare_fit()
        self.single_controller.data.fit_data() # fit is saved via signal

        # Schedule fitting the next dataset
        QtCore.QTimer.singleShot(wait, lambda: self.fit(wait=wait))

    def prepare_fit(self):
        if self.aut_w.auto_roi_checkbox.isChecked():
            roi = self.single_controller.data.recommended_roi(
                self.aut_w.auto_roi_arg.text()
            )
            self.single_controller.data.roi = roi

        if self.aut_w.initialize_checkbox.isChecked():
            self.single_controller.data.initialize_fit()
            
        if self.aut_w.peakdetect_checkbox.isChecked():
            self.single_controller.peakdetect()
            
        if self.aut_w.param_checkbox.isChecked():
            for m in self.single_controller.data.models:
                try:
                    param_name = m.prefix + self.measurement.metadata["xlabel"]
                    m.parameters[param_name].value = self.cur_dataset_param
                except KeyError:
                    pass

    def visualize_fit(self):
        x_name = self.vis_w.visualize_x.currentText()
        y_name = self.vis_w.visualize_y.currentText()
        x = np.zeros(self.count_datasets())
        y = np.zeros(self.count_datasets())
        for i, fit in enumerate(self.fits):
            try:
                x[i] = fit.best_values[x_name]
            except (KeyError, AttributeError):
                x[i] = None
            try:
                y[i] = fit.best_values[y_name]
            except (KeyError, AttributeError):
                y[i] = None

        if x_name == "index":
            x = np.array(np.arange(0, self.count_datasets()), dtype=float)
        elif x_name == self.measurement.metadata["xlabel"]:
            x = self.get_index_value()
        if y_name == "index":
            y = np.array(np.arange(0, self.count_datasets()), dtype=float)
        elif y_name == "x":
            y = self.get_index_value()


        valid_idx = np.where(((x > 0) | (x <= 0)) & ((y > 0) | (y <= 0)))
        self.visualize_controller = SingleController(data=(x[valid_idx],
                                                           y[valid_idx]),
                                                     labels = [x_name, y_name],
                                                     standalone = False)
        self.visualize_controller.show_view()


    def visualize_fit_inline(self):
        x_name = self.vis_w.visualize_x.currentText()
        y_name = self.vis_w.visualize_y.currentText()
        x = np.zeros(self.count_datasets())
        y = np.zeros(self.count_datasets())
        for i, fit in enumerate(self.fits):
            try:
                x[i] = fit.best_values[x_name]
            except (KeyError, AttributeError):
                x[i] = None
            try:
                y[i] = fit.best_values[y_name]
            except (KeyError, AttributeError):
                y[i] = None

        if x_name == "index":
            x = np.array(np.arange(0, self.count_datasets()), dtype=float)
        elif x_name == self.measurement.metadata["xlabel"]:
            x = self.get_index_value()
        if y_name == "index":
            y = np.array(np.arange(0, self.count_datasets()), dtype=float)
        elif y_name == "x":
            y = self.get_index_value()

        valid_idx = np.where(((x > 0) | (x <= 0)) & ((y > 0) | (y <= 0)))
        self.vis_w.plot(x[valid_idx], y[valid_idx], labels = [x_name, y_name])
        if self.fits[self.cur_dataset_idx] is not None:
            self.vis_w.plot_current([x[self.cur_dataset_idx]],
                                    [y[self.cur_dataset_idx]])

    def select_dataset_fit_param(self, item, points):
        """
        Select a dataset by its fit values
        """
        x = points[0].pos()[0]
        y = points[0].pos()[1]
        x_name = self.vis_w.visualize_x.currentText()
        y_name = self.vis_w.visualize_y.currentText()
        if x_name == "index":
            self.cur_dataset_idx = int(x)
            return x
        if y_name == "index":
            self.cur_dataset_idx = int(y)
            return y
        if x_name == self.measurement.metadata["xlabel"]:
            self.cur_dataset_idx = list(self.measurement.X[:, 0]).index(x)
            return x
        if y_name == self.measurement.metadata["ylabel"]:
            self.cur_dataset_idx = list(self.measurement.X[:, 0]).index(y)
            return y
            
        for i, f in enumerate(self.fits):
            try:
                if f.best_values[x_name] == x and f.best_values[y_name] == y:
                    break
            except (KeyError, AttributeError):
                pass

        self.nav_w.datasets_slider.setValue(i)
        return x

    def delete_visible_fits(self):
        """
        Delete all fits which are displayed in the inline visualization widget
        """
        view_rect = self.vis_w._plot_item.viewRect()
        x_lim = [view_rect.left(), view_rect.right()]
        y_lim = [view_rect.top(), view_rect.bottom()]
        x_name = self.vis_w.visualize_x.currentText()
        y_name = self.vis_w.visualize_y.currentText()
        
        del_x_idx = []
        del_y_idx = []
        for i, f in enumerate(self.fits):
            try:
                if f.best_values[x_name] >= x_lim[0] and f.best_values[x_name] <= x_lim[1]:
                    del_x_idx.append(i)
            except (KeyError, AttributeError):
                pass
            try:
                if f.best_values[y_name] >= y_lim[0] and f.best_values[y_name] <= y_lim[1]:
                    del_y_idx.append(i)
            except (KeyError, AttributeError):
                pass

        if x_name == "index":
            del_x_idx = np.arange(*x_lim)
        elif y_name == "index":
            del_y_idx = np.arange(*y_lim)
        elif x_name == self.measurement.metadata["xlabel"]:
            idx_lim = np.sort([np.argmin(np.abs(self.measurement.X[:, 0]-x_lim[0])),
                               np.argmin(np.abs(self.measurement.X[:, 0]-x_lim[1]))])
            del_x_idx = np.arange(*idx_lim)
        elif y_name == self.measurement.metadata["ylabel"]:
            idx_lim = np.sort([np.argmin(np.abs(self.measurement.X[:, 0]-y_lim[0])),
                               np.argmin(np.abs(self.measurement.X[:, 0]-y_lim[1]))])
            del_y_idx = np.arange(*idx_lim)
        
        del_idx = np.intersect1d(del_x_idx, del_y_idx, assume_unique=True)

        msgBox = QtGui.QMessageBox()
        msgBox.setText("This action will delete %i fit results and can not be undone." % len(del_idx))
        msgBox.addButton(QtGui.QPushButton('Delete selected fits'), QtGui.QMessageBox.AcceptRole)
        msgBox.addButton(QtGui.QPushButton('Do not delete fits'), QtGui.QMessageBox.RejectRole)
        reply = msgBox.exec_()
        
        if reply == QtGui.QMessageBox.AcceptRole:
            for i in del_idx:
                self.fits[int(i)] = None
            
        self.visualize_fit_inline()
    
    def fit_succeeded(self):
        self.fits[self.cur_dataset_idx] = self.single_controller.data.fit
        self.rois[self.cur_dataset_idx] = self.single_controller.data.roi
        self.visualize_fit_inline()

    ######################################
    # %% Saving/Recalling fit state
    def save_session(self, filename=None, overwrite=False):
        if any(self.fits):
            if not filename:
                filename = str(QtGui.QFileDialog.getExistingDirectory(
                    self.main_widget,
                    "Save Session to directory",
                    self.measurement.metadata["fname"]))
                overwrite = True
            elif os.path.isfile(filename + ".metadata.json") and not overwrite:
                raise(FileExistsError)

            if filename == '':
                return False

            for i, fit in enumerate(self.fits):
                if fit is not None:
                    self.cur_dataset_idx = i
                    fname = os.path.join(filename,
                                         (self.measurement.metadata["title"] +
                                          "." +
                                          self.measurement.metadata["xlabel"] + "=%.2e" % np.squeeze(self.measurement.X[self.cur_dataset_idx, 0]) +
                                          ".i=%04d" % i)
                                          )
                    self.single_controller.save_fit(filename=fname + ".models.json",
                                                    overwrite=overwrite)

            # self.measurement.save(path=filename, overwrite=overwrite)
        else:
            raise Exception("No fit has succeeded yet.")


    def load_session(self, filename=None):
        if not filename:
            filename = str(QtGui.QFileDialog.getExistingDirectory(
                self.main_widget,
                "Load session from directory",
                self.measurement.metadata["fname"]))
        elif os.path.isfile(filename):
            raise(FileExistsError)

        if filename == '':
            return False

        fnames = glob(os.path.join(filename, '') + "*.models.json")
        for fname in fnames:
            i = int((fname.split(".")[-3])[2:])
            self.fits[i] = self.single_controller.load_fit(filename=fname,
                                                           emit=False)

    def save_measurement(self, filename=None, overwrite=True):
        postfix = '.measurement.json'
        if not filename:
            filename = str(QFileDialog_compatibility(QFileDialog.getSaveFileName,
                self.main_widget,
                "Save Measurement",
                self.measurement.metadata["title"] + postfix))
            if postfix in filename:
                filename.replace(postfix, "")

        elif os.path.isfile(filename) and not overwrite:
            raise(FileExistsError)

        if filename == '':
            return False

        self.measurement.save(filename=filename, overwrite=overwrite)

    def load_measurement(self, filename=None):
        if not filename:
            filename = str(QFileDialog_compatibility(QFileDialog.getOpenFileName,
                self.main_widget, "Load Measurement", '',
                filter="*.measurement.json"))

        if filename is not '':
            self.measurement = None
            with open(filename, "r") as f:
                self.measurement = jsonpickle.loads(f.read())
