# -*- coding: utf-8 -*-
from __future__ import absolute_import
import sympy as s  # prevent import errors w/ PyQt if sympy is used in a model
import matplotlib
matplotlib.use("Qt4Agg")
from guidata.qt import QtGui
import sys

from peakit.controller.MeasurementController import MeasurementController

def run():
    app = QtGui.QApplication(sys.argv)
    main_controller = MeasurementController()
    main_controller.show_view()
    app.exec_()

    return main_controller


if __name__ == '__main__':
    main_controller = run()